require('dotenv').config()
const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');

const jwt = require('jsonwebtoken');


/** import models */
const {User}  = require('../../model/SignupUser');
const {Category, validateCategry} = require("../../model/Category")
const {Project} = require("../../model/Project")
const {Newsletter,validateNewsletter} = require("../../model/Newsletter")
const {BlogCategory,validateBlogCategry} = require("../../model/BlogCategory")
const {Post}  = require('../../model/Post');

/** import models */



/** AUTH & ADMIN */
const auth = require("../../middleware/auth")
const admin = require("../../middleware/admin")
const _ = require("lodash")
/** AUTH & ADMIN */



/** NEWSLETTER **/
router.get("/newsletter", auth, admin, async(req, res) => {
    try{
        const newsletter = await Newsletter.find()
                                           .sort({date_created:-1})
        res.json(newsletter)
    }catch(e){
        if(e) res.status(400).json({ msg: err.message})        
    }
})


/** GET USER */

router.get("/users",auth,admin,async(req, res)=> {
    
    try{
        const user = await User.find()
                                .sort({timestamp:-1})
        res.json(user)
    }catch(e){
        if(e) res.status(400).json({ msg: err.message})
    }
;
})



/** GETTING A PARTICULAR USER */
router.get("/user/:userID",auth, admin,async (req, res)=> {
    try{
         const user = await User.findById(req.params.userID)
         res.json(user)
    }catch(err){
        return res.status(400).json({ msg: err.message})
    }

       
})



router.get("/users/length",(req, res)=> {
    User.find()
        .then((user, err)=> {
            if(err) return res.status(400).json({ msg: err.message})
            res.json(user.length)
        }) 
    .catch(err => res.status(404).json({ failed: err }));
})

/** DELETE USER */
router.delete('/delete/:userID',auth, admin , async(req, res) => {
    try{
        // 5f17314637065f2c4dc1f6d0
        const user = await User.findByIdAndRemove(req.params.userID)
        res.json(user)
    }catch(err){
        return res.status(400).json({ msg: err.message})
    }
  });
/** END DELETE USER */

/** FLAG USER */
// 5f16e4a300269b37cedb4641
router.put('/flag/:userID',auth, admin, async(req, res) => {
        try{
            const user = await User.findByIdAndUpdate(req.params.userID, {
                $set: {
                    flag: true
                  }
            })
            res.json(user)
 
        }catch(err){
            return res.status(400).json({ msg: err.message})
        }
            
  });

  
/** END FLAG USER */

/**  GET FLAGGED USERS*/

router.get("/users/flag",auth, admin, async (req, res)=> {
    try{
      const users = await  User.find().where("flag").equals(true)
        res.json(users)
    }catch(err){
        return res.status(400).json({ msg: err.message})
    }
       
})



/** END GET FLAGGED USERS */

/**  GET FLAGGED USERS LENGTH*/

router.get("/users/flag/length", auth,admin,async(req, res)=> {
    try{
        const users = await User.find().where("flag").equals(true)  
        res.json(users.length)
    }catch(err){
        return res.status(400).json({ msg: err.message})
    }
       
})



/** END GET FLAGGED USERS  LENGTH */

/*
START CATEGORY
*/
router.post("/create-project-category", auth, admin,async(req, res)=> {
    try{
        const { error } = validateCategry(req.body); 
        if (error) return res.status(400).json({msg: error.details[0].message});
    
        let category = await Category.findOne({
            category_name:req.body.category_name
        })
        
        if(category) return res.status(400).json({msg:'Category already registered.'})
        const {category_name} = req.body
        // console.log(category_name.toLowerCase())
        category= new Category({
            category_name:category_name.toLowerCase()
        });
        await category.save()
        res.json(category)
    }catch(err){
        return res.status(400).json({ msg: err.message})
    }
})


router.post("/create-blog-category", auth, admin,async(req, res)=> {
    try{
        const { error } = validateBlogCategry(req.body); 
        if (error) return res.status(400).json({msg: error.details[0].message});
    
        let category = await Category.findOne({
            category_name:req.body.category_name
        })
        
        if(category) return res.status(400).json({msg:'Category already registered.'})
        const {category_name} = req.body
        // console.log(category_name.toLowerCase())
        category= new BlogCategory({
            category_name:category_name.toLowerCase()
        });
        await category.save()
        res.json(category)
    }catch(err){
        return res.status(400).json({ msg: err.message})
    }
})

router.get("/get-project-category",auth, admin,async(req, res)=> {
    try{
        const categories = await  Category.find()
        res.json(categories)
    }catch(err){
        return res.status(400).json({ msg: err.message})
    }
})
router.get("/get-blog-category",auth, admin,async(req, res)=> {
    try{
        const categories = await  BlogCategory.find()
        res.json(categories)
    }catch(err){
        return res.status(400).json({ msg: err.message})
    }
})


router.get("/get-project-category-length", auth, admin, async(req, res) => {
    try{
        const categories = await Category.find()
        res.json(categories.length)
    }catch(err){
        return res.status(400).json({ msg: err.message})
    }
})


router.delete("/:id/delete-project-category", auth, admin, async(req, res)=>{
    try{
        const category = await   Category.findByIdAndDelete(req.params.id)
        res.json({msg:"DELETED SUCCESSFULLY"})
    }catch(err){
        return res.status(400).json({ msg: err.message})
    }
})

/*
END CATEGORY
*/


/*
 PRODUCT
    - GET PROJECTS
    - FLAG PRODUCTS
    - DELETE PRODUCTS
    
*/

router.get("/projects",auth,admin,async(req, res)=> {
    // try{

    // }catch(2)
    try{
        const projects = await  Project.find()
        res.json(projects)
    }catch(err){
        return res.status(400).json({ msg: err.message})
    }
})

router.put("/get-projects/draft",auth, admin, async(req, res) => {
    try{
        const projects = await Project.find()
                                .where("status")
                                .equals("draft")

        res.json(projects)
    }catch(e){
        return res.status(400).json({msg:err.message})
    }
})

router.get("/get-project/:slug",auth,admin,async(req, res)=> {
    try{
        const project = await Project.findOne({slug:req.params.slug})
        res.json(project)
    }catch(e){
        return res.status(400).json({msg:err.message})
    }
})

// router.put("/get-project/:slug/publish",auth,admin,async(req, res)=> {
//     try{
//         const project = await Project.findOneAndUpdate({slug:req.params.slug}, {
//             $set:{
//                 "status":"publish"
//             }
//         })
//         res.json(project)
//     }catch(e){
//         return res.status(400).json({msg:err.message})
//     }
// })

/**ADMIN:  PUBLISH PROJECT */
router.put("/get-project/:slug/publish",auth, admin,async(req, res)=> {
    try{
        const project = await Project.findOneAndUpdate({slug:req.params.slug}, {
            $set:{
                "status":"publish",
                verified:true
            }
        })
        res.json(project)
    }catch(e){
        return res.status(400).json({msg:err.message})
    }
})

router.put("/get-post/:slug/publish",auth, admin,async(req, res)=> {
    try{
        const post = await Post.findOneAndUpdate({slug:req.params.slug}, {
            $set:{
                "status":"publish"
            }
        })
        res.json(post)
    }catch(e){
        return res.status(400).json({msg:err.message})
    }
})


/** STAFF: PUBLISH PROJECT */
router.get("/projects/length",auth, admin, async(req, res)=>{
    try{
        const projects = await Project.find()
        res.json(projects.length)
    }catch(e){
        return res.status(400).json({msg:e.message})
    }
})

router.get("/projects/totalBalance",auth, admin, async(req, res)=>{
    try{
        const getBalance = await Project.aggregate([{
            $group: {
                 _id: null,
                 // "project_current_balance": { "$sum": "$project_current_balance" }
                 count: { $sum: "$project_current_balance" }
             }
         }
         ])
         res.json(getBalance)
    }catch(e){
        return res.status(400).json({msg:e.message})
    }
    })
    
router.put("/flag-project/:slug",async(req, res)=>{
        try{
            const project = await Project.findOneAndUpdate({slug:req.params.slug},{
                $set: {
                  flag: true
                }
              })
              res.json({msg:"SUCCESSFULLY FLAGGED"})
        }catch(e){
            res.status(403).json({msg:e.message})
        }
})


router.delete("/delete-project/:slug",auth, admin, async (req, res)=>{
  try{
    const project = await Project.findOneAndDelete({slug:req.params.slug})
    res.json({msg:"DELETED"})
  }catch(e){
      return res.status(400).json({msg:e.message})
  }
})


/* END
 PRODUCT
    - FLAG PRODUCTS
    - DELETE PRODUCTS
    
*/

/*
CAMPAIGNS 
*/



  /** VERIFY A CAMPAIGN*/
router.put('/verify/:slug',async(req, res) => {
    try{
       const project = await Project.findOneAndUpdate({slug : req.params.slug}, {
            $set: {
                verified: true,
                "status":"publish"
              }
        })
        res.json({msg:"UPDATED"})
    }catch(e){
        return res.status(400).json({msg:e.message})
    }

  });



// make a user an admin
  router.put('/makeAdmin/:id',auth, admin, async(req, res) => {
    try{
        const user =     User.findByIdAndUpdate(req.params.id,{
            $set :{
                isAdmin : true
            }
        })   
        res.json({msg:"ADMIN PRIVILEGES ADDED"})
    }catch(e){
        return res.status(400).json({msg:e.message})
    }
  });

  
  
/** END FLAG USER */


module.exports = router;
// 5e84d635f467b16002977e9f

//categories
/*

5e84da6bb9ad37673fe36a2d
5e84da74b9ad37673fe36a2e
5e84dae2b45cb367c9f1aab0
5e84daebb45cb367c9f1aab1
5e84db5174369a6870a06033
5e84db9d59c57168bfd0f314
5e84de71865b926bbb47d6e9
*/