const express = require("express");
const router = express.Router();
const multer = require('multer')
const fs = require('fs');
const request = require("request");
const _ = require("lodash");
const api_Key = '5779602fb87b8fb328ed684d05ea7339-7fba8a4e-5517562d'
const domain = 'elesarr.com';
var mailgun = require('mailgun-js')({apiKey: api_Key, domain: domain});

const { initializePayment, verifyPayment } = require("../../config/paystack")(
    request
  );
  


const storage = multer.diskStorage({
    filename: (req, file, cb) => {
      const { userID } = req.body
      console.log("FILE", file.mimetype)
      if(file.mimetype === 'application/pdf'){
        cb(null, `organisations-campaign-pitchDeck-files-UserId-${userID}-File-${Date.now()}.pdf`)
      }
      else {
        cb(null, `organisations-campaign-files-UserId-${userID}-Image-${Date.now()}.png`)

      }
    }
    })
    
    const fileFilter =(req, file, cb)=>{
      // reject a file
      if(file.mimetype === 'image/jpeg' || file.mimetype == "image/jpg" || file.mimetype == 'image/png' || file.mimetype == 'application/pdf' || file.mimetype == 'file/pdf' || file.mimetype == 'file/ppt'){
        cb(null, true)
      }else{
        cb(null, false)
  
      }
    } 
    const upload = multer({ 
        storage:storage,
        limits:{
          fileSize:1024*1024*5
        },
        fileFilter:fileFilter
  })
  

const cloudinary = require("cloudinary");
cloudinary.config({
  cloud_name: "elesarr",
  api_key: "767878828592729",
  api_secret: "vnd_jkBSI05Y0_YzNxwgpoJo1rI"
});
  
const Project = require("../../model/Project"); // require project model




var cpUpload = upload.fields([
    { name: 'image_slide_1', maxCount: 1 }, 
    { name: 'image_slide_2', maxCount: 1 },
    { name: 'image_slide_3', maxCount: 1 },
    
])




router.post("/create-campaign",cpUpload,async(req, res) => {


  const {
    userID,
    project_name,
    project_description,
    // pitch_deck,
    fb_url,
    linkedin,
    inst_url,
    video_url,
    project_deadline,
    project_goal,
    useCrypto,
    email,
    username,
    category
    // project_current_balance,
    // email
  } = req.body;
  const{
    image_slide_1,
    image_slide_2,
    image_slide_3,
    // pitch_deck,
  }= req.files
  if (
    !userID ||
      !project_name ||
      !project_description ||
      !fb_url ||
      !linkedin ||
      !inst_url ||
      // !video_url ||
      !project_deadline ||
      !useCrypto ||
      !project_goal ||
      !email || 
      !username ||
      !category)
   {
    return res.status(400).json({ msg: "Please enter all fields" });
  }

  if (
    !image_slide_1 ||
      !image_slide_2 ||
      !image_slide_3 
      // !pitch_deck 
      )
   {
    return res.status(400).json({ msg: "Please enter all fields" });
  }

  var image1
  var image2 
  var image3
  var pitchDeck



  await cloudinary.v2.uploader.upload(
      image_slide_1[0].path,
      {public_id:`campaign/organisation/slide1/${userID}`} ,function(err, result) {
    req.body.image = result.secure_url;
    req.body.imageId = result.public_id;
    console.log("SECURED",req.body.image)
    console.log("PUBLIC",req.body.imageId)
    if (err) {
    res.json(err.message);
    }else{
      image1  = result.secure_url
    }
   
  })

               


  await cloudinary.v2.uploader.upload(
    image_slide_2[0].path,
    {public_id:`campaign/organisation/slide2/${userID}`},
    function(err, result) {
    req.body.image = result.secure_url;
    // add image's public_id to image object
    req.body.imageId = result.public_id;
    
    if (err) {
    res.json(err.message);
    }else{
      image2  = result.secure_url
    }
  })



 await cloudinary.v2.uploader.upload(
   image_slide_3[0].path,
  {public_id:`campaign/organisation/slide3/${userID}`},
  function(err, result) {
    req.body.image = result.secure_url;
    req.body.imageId = result.public_id;
    
    if (err) {
    res.json(err.message);
    }else{
      image3  = result.secure_url
 

      /*
      HIT THE PROJECT TABLE
      */
 try {

  const newProject = new Project({
    userID: userID,
    project_name: project_name,
    project_description: project_description,
    image_slide_1: image1,
    image_slide_2: image2,
    image_slide_3: image3,
    pitch_deck: '',
    fb_url: fb_url,
    linkedin: linkedin,
    inst_url: inst_url,
    video_url: video_url,
    project_deadline: project_deadline,
    project_goal: project_goal,
    useCrypto:useCrypto,
    email:email,
    username:username,
    category:category
    // email: email
  });
  newProject.save().then(done =>{
    res.status(200).json({
      succes: done
    })}
  );
 }catch(e){
   if(e) console.log(e)
 }

    }
  })



  });
  

  /*
  GET ORGANISATIONS CAMPAIGN
  */
 router.get("/:id/get-campaigns", (req, res) => {
    Project.find({userID:req.params.id})
      .then(
        projects => {
          if(projects){
            res.json(projects);
          }else {
            res.json({
              message:"YOU HAVEN't CREATED ANY CAMPAIGNS"
            })
          }
        }
      )
      .catch(err => res.status(204).json({ success: err }));
  });
  


  /*
  GET SINGLE ORGANISATIONS CAMPAIGN
  */
  
 router.get("/:id/get-single-campaign", (req, res) => {
    const projectID = req.params.id;
    console.log(projectID);
    Project.findById(projectID).then(
      project => {
        if (!projectID) {
          res.json({ msg: "No Project with this id exists" });
        }
        // projects => {
        res.json({
          project: project
        });
        // }
      }
    );
  });
  






/*
UPDATE CAMPAIGN
*/




// @route   delete api/organisationproject/campaign/update/:id
// @desc    update campaign route
// @access  Public


// @parameters 
/**
 * 
 * userID
 * project_name
 * project_description
 * fb_url
 * linkedin
 * inst_url
 * video_url
 * project_deadline
 * project_goal
 * useCrypto
 * email
 * username
 * category
 * image_slide_1
 * image_slide_2
 * image_slide_3,
 */


router.put("/campaign/update/:id", cpUpload ,(req, res) => {



  var id = req.params.id;
    const {
      userID,
      project_name,
      project_description, 
      fb_url,
      linkedin,
      inst_url,
      video_url,
      email,
      category
    } = req.body;
    const {
      image_slide_1,
      image_slide_2,
      image_slide_3,
    }= req.files
    var image1
    var image2 
    var image3

    Project.findById(id).then(async project => {
      if (project.userID !== userID) {
        res.status(400).json({ msg: "unauthorized" });
      }

      if( !image_slide_1 || !image_slide_2 || !image_slide_3){
        image1= project.image_slide_1
        image2= project.image_slide_2
        image3= project.image_slide_3
        pitchDeck=   project.pitch_deck
       
        Project.findByIdAndUpdate(id, {
            $set: {
              project_name: project_name,
              project_description: project_description,
              image_slide_1: image1,
              image_slide_2: image2,
              image_slide_3 : image3,
              pitch_deck:pitchDeck,
              fb_url: fb_url,
              linkedin: linkedin,
              inst_url: inst_url,
              video_url: video_url,
              date_updated:new Date(),
              email:email,
              category:category
            }
          })
            .then(() => Project.findById(id))
            .then(project => res.json(project))
            .catch(err => res.status(204).json({ success: err }));      
      } else {
            await cloudinary.v2.uploader.upload(
              image_slide_1[0].path,
              {public_id:`campaign/individual/slide1/${userID}`} ,function(err, result) {
              req.body.image = result.secure_url;
              req.body.imageId = result.public_id;
              if (err) {
                res.json(err.message);
              }else{
                image1  = result.secure_url
              }

          })

        // IMAGE SLIDE 2
        await cloudinary.v2.uploader.upload(
          image_slide_2[0].path,
          {public_id:`campaign/individual/slide2/${userID}`},
          function(err, result) {
          req.body.image = result.secure_url;
          req.body.imageId = result.public_id;
          
          if (err) {
          res.json(err.message);
          }else{
            image2  = result.secure_url
          }
        })


      // IMAGE SLIDE 3
      await cloudinary.v2.uploader.upload(
        image_slide_3[0].path,
      {public_id:`campaign/individual/slide3/${userID}`},
      function(err, result) {
        req.body.image = result.secure_url;
        // add image's public_id to image object
        req.body.imageId = result.public_id;

        if (err) {
        res.json(err.message);
        }else{
          image3  = result.secure_url

          Project.findByIdAndUpdate(id, {
            $set: {
              project_name: project_name,
              project_description: project_description,
              image_slide_1: image1,
              image_slide_2: image2,
              image_slide_3 : image3,
              pitch_deck:pitchDeck,
              fb_url: fb_url,
              linkedin: linkedin,
              inst_url: inst_url,
              video_url: video_url,
              email:email,
              category:category
            }
          })
            .then(() => Project.findById(id))
            .then(user => res.json({ updated: user }))
            .catch(err => res.status(204).json({ success: err }));
        
        }
      })


    

      }

    
    })

  });



  /*
  DELETING CAMPAIGN
  */
 router.delete("/:userId/delete/:id", (req, res) => {
    const userId = req.params.userId
    Project.findById(req.params.id)
      .then(project =>{
          if(project.userID == userId){
            project.remove().then(() => res.json({ success: true }))
          }  else{
            res.status(403).json("NOT AUTHORIZED TO DELETE PROJECT ")
          }
      })
      .catch(err => res.status(404).json({ success: false }));
  });


  
module.exports = router;


