/** import libraries */
require("dotenv").config();
const express = require("express");
const router = express.Router();
const multer = require('multer')
const fs = require('fs');
const request = require("request");
const _ = require("lodash");
/** import libraries */



/** MODELS IMPORT */
const {Guest}= require("../../model/Guest")
const {Notification} = require("../../model/Notification")
const {Project,validateProjectImages,validateProject} = require("../../model/Project");
const {Category} = require("../../model/Category")
const {Update, validateUpdate} = require("../../model/Update")
const {ProjectComment,validateProjectComment} = require("../../model/ProjectComments")
const {ProjectFaq, validateProjectFaq} = require("../../model/ProjectFaq")

/** MODEL IMPORT */



/** MAILGUN CONFIGURATION */
const api_Key = process.env.MAILGUNKEY
const domain = 'elesarr.com';
var mailgun = require('mailgun-js')({apiKey: api_Key, domain: domain});
/** END MAILGUN CONFIGURATION */

/** IMPORT AUTH CONFIG */
const auth = require("../../middleware/auth") // import auth middleware
const admin = require("../../middleware/admin")

/** IMPORT AUTH CONFIG */


/** CLOUDINARY CONFIGURATION */
const cloudinary = require("cloudinary");
cloudinary.config({
  cloud_name: "elesarr",
  api_key: "767878828592729",
  api_secret: "vnd_jkBSI05Y0_YzNxwgpoJo1rI"
});
const storage = multer.diskStorage({
  filename: (req, file, cb) => {
    const { userID } = req.body
    console.log("FILE", file.mimetype)
    if(file.mimetype === 'application/pdf'){
      cb(null, `individual-campaign-pitchDeck-files-UserId-${userID}-File-${Date.now()}.pdf`)
    }
    else {
      cb(null, `individual-campaign-files-UserId-${userID}-Image-${Date.now()}.png`)

    }
  }
  })
  const fileFilter =(req, file, cb)=>{
    // reject a file
    if(  file.mimetype === 'image/jpeg' ||
         file.mimetype == "image/jpg" || 
         file.mimetype == 'image/png' || 
         file.mimetype == 'application/pdf' || 
         file.mimetype == 'file/pdf' || 
         file.mimetype == 'application/vnd.ms-powerpoint' || 
         file.mimetype ==  "application/vnd.openxmlformats-officedocument.presentationml.presentation"){
      cb(null, true)
    }else{
      cb(null, false)
    }
  } 
  const upload = multer({ 
      storage:storage,
      limits:{
        fileSize:1024*1024*5
      },
      fileFilter:fileFilter
})
var cpUpload = upload.fields([
  { name: 'image_slide_1', maxCount: 1 }, 
  { name: 'image_slide_2', maxCount: 1 },
  { name: 'image_slide_3', maxCount: 1 },
  // { name: 'pitch_deck', maxCount: 1 },    
])
/** CLOUDINARY CONFIGURATION */



/** PUSHER JS CONFIGURATION */
var Pusher = require("pusher");
var pusher = new Pusher({
  appId: '1004110',
  key: '2347f2ea036eda8b245e',
  secret: 'b072af717d87f7f792a5',
  cluster: 'eu',
  encrypted: true
});

/**  PUSHER JS CONFIGURATION */

/**
 * @route   POST api/project/create-campaign
 * @desc    Create New Campaign
 * @access  Public
 * @params
 *  * 
 *    userID
 *    project_name
 *    project_description
 *    fb_url
 *    linkedin
 *    inst_url
 *    video_url
 *    project_deadline
 *    project_goal
 *    useCrypto
 *    email
 *    username
 *    category
 *    image_slide_1
 *    image_slide_2
 *    image_slide_3,
 */
 





router.post("/create-campaign",auth, cpUpload,async (req, res) => {  

  const {
    // userID,
    project_name,
    project_description,
    fb_url,
    linkedin,
    inst_url,
    video_url,
    project_deadline,
    project_goal,
    category
  } = req.body;

  /** 3 IMAGES */
  const{
    image_slide_2,
    image_slide_3
  }= req.files
    /** 3 IMAGES */
    // validateProject
  if (
    // !userID ||
      !project_name ||
      !project_description ||
      !fb_url ||
      !linkedin ||
      !inst_url ||
      // !video_url ||
      !project_deadline ||
      // !useCrypto ||
      !project_goal ||
      !category)
   {
          /** CHECKS IF USER ADDED ALL FIELD */
    return res.status(400).json({ msg: "PLEASE ENTER ALL FIELDS" });
  }

    
  if (
      !image_slide_2 ||
      !image_slide_3 
      )
   {
     /** CHECKS IF USER UPLOADED ALL IMAGES */
    return res.status(400).json({ msg: "PLEASE UPLOAD ALL IMAGES" });
  }

  var newProjectName = project_name.replace(/[\|&;\$%@".:<>\(\)\+,]/g, "");

  var image2  // declaration 
  var image3 // declaration

  /** CLOUDINARY IMPLEMENTATION */
 
  await cloudinary.v2.uploader.upload(
    image_slide_2[0].path,
    {public_id:`campaign/individual/slide2/${req.user.id}`},
    function(err, result) {
      /** UPLOADING IMAGE 2 */
    req.body.image = result.secure_url;
    // add image's public_id to image object
    req.body.imageId = result.public_id;
    if (err) {
      return res.status(400).json({ msg: err.message});
    }else{
      image2  = result.secure_url
    }
  })

 await cloudinary.v2.uploader.upload(
   image_slide_3[0].path,
  {public_id:`campaign/individual/slide3/${req.user.id}`},
  function(err, result) {
    /** UPLOADING IMAGE 3 */
    req.body.image = result.secure_url;
    req.body.imageId = result.public_id;    
    if (err) {
      return res.status(400).json({ msg: err.message});
    }else{
      image3  = result.secure_url
          try {
                const newProject = new Project({
                  userID: req.user.id,
                  project_name: newProjectName,
                  project_description: project_description,
                  image_slide_2: image2,
                  image_slide_3: image3,
                  // pitch_deck: '',
                  fb_url: fb_url,
                  linkedin: linkedin,
                  inst_url: inst_url,
                  video_url: video_url,
                  project_deadline: project_deadline,
                  project_goal: project_goal,
                  // useCrypto:useCrypto,
                  category:category
                });  // initialize data
                Project.findOne({project_name:project_name})
                  .then((project)=>{
                    if(project){
                      return res.status(400).json({ msg: "PROJECT WITH THE SAME PROJECT NAME EXISTS!!"});
                    }
                    newProject.save().then(done =>{
                      const notifData = new Notification({
                        ownerID: req.user.id,
                        message: `
                          Sucessfully Created Campaign '${done.project_name}'
                          `,
                        link: `/dashboard/projects/${req.user.id}`,
                      });
                      notifData.save();
                      const pusherData = {
                        // username:username,
                        userID: req.user.id,
                        project_name: project_name,
                      };
                      pusher.trigger("Elesarr", "CAMPAIGN_CREATED", {
                        pusherData,
                      });  // trigger events
                      res.status(200).json(done)
                    
                    }
                    )
                    .catch(e=>{
                      if(e) return res.status(400).json({
                           msg: e.message
                          //  "ERROR Contact Administrator!!! or email customercare@elesarr.com" 
                          
                          });
                    })
                  })
                }catch(e){
                  if(e) return res.status(400).json({ 
                    msg:e.message 
                    // "ERROR Contact Administrator!!! or email customercare@elesarr.com"
                  });
                }

                console.log("notification reached");
              
    }
      /** CLOUDINARY IMPLEMENTATION */

  })
});

/*
UPDATE CAMPAIGN
*/

/**
 * @method PUT
 * @route   PUT api/project/campaign/update/:id
 * @desc    Create New Campaign
 * @access  Public
 * @params
 *  * 
 *    userID
 *    project_name
 *    project_description
 *    fb_url
 *    linkedin
 *    inst_url
 *    video_url
 *    email
 *    category
 */

router.put("/campaign/update/:slug", auth,async (req,res) =>{
  /** UPDATE CAMPAIGN */
    const {
      project_name,
      project_description, 
      fb_url,
      linkedin,
      inst_url,
      video_url,
      category
    } = req.body; // form data for request body
    var newProjectName = project_name.replace(/[\|&;\$%@".:<>\(\)\+,]/g, "");
   
    Project.findOne({slug:req.params.slug}).then(async project => {
      /** CHECKS IF THE PROJECTS EXISTS */
      if (project.userID != req.user.id) {
        console.log("PROJECT USER ID", project.userID)
        console.log("LOGGED IN USER", req.user.id)
        return res.status(400).json({ msg: "unauthorized User" });
      }
      const notifData = new Notification({
        ownerID: req.user.id,
        message: `
          Sucessfully Updated Campaign '${project_name}'
          `,
        link: `/dashboard/projects/${project.userID}`,
      });
      notifData.save();
        pusher.trigger('Elesarr', 'CAMPAIGN_UPDATED', {
          // username:username,
          userID: req.user.id,
          project_name: newProjectName,
        });  
        
        Project.findOneAndUpdate({slug:req.params.slug}, {
            $set: {
              project_name: newProjectName,
              project_description: project_description,
              fb_url: fb_url,
              linkedin: linkedin,
              inst_url: inst_url,
              video_url: video_url,
              category:category,
              date_updated:new Date()
            }
          })
            .then(project =>{              
              res.json(project)
            })
            .catch(err =>{
              console.log(err.message)
              return res.status(400).json({ msg: "ERROR Contact Administrator!!! or email customercare@elesarr.com" });
            })          
    })  
    .catch(err =>{
      console.log(err.message)
      return res.status(400).json({ msg: err.message });
    })       

})

/*
END UPDATE CAMPAIGN
*/



/** UPDATE CAMPAIGN IMAGES */
/**
 * @method PUT
 * @route   PUT api/project/campaign/update-images/:id
 * @desc    Create New Campaign
 * @access  Public
 * @params
 *  * 
 * image_slide_1
 * image_slide_2
 * image_slide_3
 */

router.put("/campaign/update-images/:slug",auth ,cpUpload, async (req,res) =>{
  /** UPDATE CAMPAIGN IMAGES */

      // const {
      //   userID
      // }= req.body
      // console.log("USER ID",userID)
      // console.log(`campaign/individual/slide1/${userID}`)
      // console.log(`campaign/individual/slide2/${userID}`)
      // console.log(`campaign/individual/slide3/${userID}`)
      
        const {

          image_slide_2,
          image_slide_3,
        }= req.files
        var image1
        var image2 
        var image3
 
    Project.findOne({slug:req.params.slug}).then(async project => {
      /** CHECKS IF THE PROJECTS EXISTS */
      if (project.userID != req.user.id) {
        res.status(400).json({ msg: "unauthorized" });
      }

      if( !image_slide_2 || !image_slide_3){
        res.status(400).json({ msg: "PLEASE UPLOAD ALL IMAGES" });  
      } 

      /** CLOUDINARY IMAGES */
      // IMAGE SLIDE 1
      try{



                // IMAGE SLIDE 2
      await cloudinary.v2.uploader.upload(
        image_slide_2[0].path,
        {public_id:`campaign/individual/slide2/${req.user.id}`},
        function(err, result) {
        req.body.image = result.secure_url;
        req.body.imageId = result.public_id;
        if (err) {
        res.json(err.message);
        }else{
          image2  = result.secure_url
        }
      })


    // IMAGE SLIDE 3
    await cloudinary.v2.uploader.upload(
      image_slide_3[0].path,
     {public_id:`campaign/individual/slide3/${req.user.id}`},
     function(err, result) {
       req.body.image = result.secure_url;
       req.body.imageId = result.public_id;
       if (err) {
       res.json(err.message);
       }else{
         image3  = result.secure_url  
         try{
          Project.findOneAndUpdate({slug:req.params.slug}, {
            $set: {
              image_slide_2: image2,
              image_slide_3 : image3
            }
          })
            .then((project,err) =>{
              /* NOTIFICATION*/
              const notifData = new Notification({
                ownerID: req.user.id,
                message: `
                  Sucessfully Updated Campaign with ID '${req.params.slug}' Images
                  `,
                // link: `/dashboard/`,
              });
              notifData.save();
                pusher.trigger('Elesarr', 'CAMPAIGN_UPDATED_IMAGES', {
                  // username:username,
                  userID: req.params.slug,
                  // project_name: project_name,
                });
              res.json({ updated: project })
            })
            .catch((err) => {
              return res.status(400).json({ msg: err.message})
            })    
         }catch(e){
           if(e){
            return res.status(400).json({ msg: "ERROR Contact Administrator!!! or email customercare@elesarr.com" });
          }
         }
       }
     })

      }catch(e){
        res.status(400).json({msg:e.message})
      }



           /** CLOUDINARY IMAGES */
    })  
})
/** UPDATE CAMPAIGN IMAGES */

/*
  GETS ALL PROJECTS OF A PARTICULAR USER
*/


router.get("/get-campaigns",auth,async (req, res) => {
  try{
    const campaign = await Project.find({userID:req.user.id})
    res.json(campaign)
  
  }catch(err){
    return res.status(400).json({ msg: err.message})
  }

});


router.get("/get-campaigns/length",auth,async (req, res) => {
  try{
    const campaign = await Project.find({userID:req.user.id})
    res.json(campaign.length)
  }catch(err){
    return res.status(400).json({ msg: err.message})
  }
});

/*
  GETS ALL PROJECTS OF A PARTICULAR USER
*/

/*
  GET ALL CAMPAIGNS
*/


// 5f16e4a300269b37cedb4641

router.get("/campaigns", async(req, res) => {
  try{
    // console.log("HELLO WORL")
    const campaigns = await   Project.find()
                                      .where('flag')
                                      .equals(false)
                                      .where("status")
                                      .equals("publish")
                                      .sort({date_created : -1})
                                      .populate("category")
                                      .populate("userID")
    //                                   .populate("like")
    res.json(campaigns)
  }catch(err){
    return res.status(400).json({ msg: err.message})
  }

})

/* GET RECENT CAMPAIGNS */
router.get("/campaigns/recent", async(req, res) => {
  try{
    const campaigns =   await Project.find()
                                      .limit(Number(3))
                                      .where("status")
                                      .equals("publish")
                                      .sort({date_created: -1})
                                      .populate("category")
                                      // .populate("like")
    res.json(campaigns)
  }catch(err){
    return res.status(400).json({ msg: err.message})
  }
        
})


/*
  GET A PARTICULAR PROJECT (SINGLE PROJECT ROUTE)
*/


/** UPDATE THIS ROUTE USING SLUG */

router.get("/:slug/get-single-campaign",async(req, res) => {
  const projectID = req.params.slug;
  // console.log(projectID);
  try{
  const campaign = await  Project.findOne({slug:req.params.slug})
                                 .populate("updates")
                                 .populate("userID")
                                 .populate("category")
                                 .populate("faq")
                                 .populate("like")
                                 .populate("project_comments")
                                 .populate({
                                   path:"project_comments",
                                   populate:{path:"owner"}
                                 })
                                //  .populate(function(err, users){
                                //    ProjectComment.p
                                //  })
                                //  .exec((err, users) => {
                                //    ProjectComment.populate("owner")
                                //  })
    res.json(campaign)
  }catch(err){
    return res.status(400).json({ msg: err.message})

  }

});



router.put("/:slug/verified",async(req, res) => {
  try{
    const campaign = await   Project.findOneAndUpdate({slug:req.params.slug}, {
      $set:{
        "verified":true
      }
    })
    res.json({msg:"CAMPAIGN VERIFIED"})
  }catch(e){
    return res.status(400).json({ msg: err.message})
  }
});








/** ADD CAMPAIGN CREATOR UPDATE TO CAMPAIGN */
router.post("/:slug/update", auth,async(req, res)=>{
  // const {content, projectID, userID} = req.body
  try{
    const { error } = validateUpdate(req.body); 
    if (error) return res.status(400).json({msg: error.details[0].message});
    let update = new Update({
      content:req.body.content,
      projectID:req.body.projectID
    })
    update.save()
    let project = await Project.findOneAndUpdate({slug:req.params.slug},
      { $push: { updates: update._id } },
      { new: true, useFindAndModify: false }
      )
      res.json(project)
  }catch(e){
    return res.status(400).json({msg: e.message})
  }
})


router.post("/:slug/comment", auth,async(req, res)=>{
  // const {content, projectID, userID} = req.body
  try{
    const { error } = validateProjectComment(req.body); 
    if (error) return res.status(400).json({msg: error.details[0].message});
    let comment = new ProjectComment({
      content:req.body.content,
      project:req.body.project,
      owner:req.user.id
    })

    comment.save()
    let project = await Project.findOneAndUpdate({slug:req.params.slug},
      { $push: { project_comments: comment._id } },
      { new: true, useFindAndModify: false }
      )  .populate("updates")
      .populate("userID")
      .populate("category")
      .populate("faq")
      .populate("like")
     //  .populate("project_comments")
      .populate({
        path:"project_comments",
        populate:{path:"owner"}
      })

      // ==========================NOTIFY THE OWNER OF THE CAMPAIGN THAT HE HAS A NEW COMMENT ==========
    const notifData = new Notification({
      ownerID: project.userID,
      message: `
        Your Campaign, ${project.project_name} Just Received A New Comment
        `,
        link : `/campaigns/${req.params.slug}`
    });

    // save the notification
    await notifData.save()
    const pusherData = {
      // username:username,
      msg:"COMMENT CREATED",
      slug:project.slug
    };
    await pusher.trigger("Elesarr", "COMMENT_CREATED", {
      pusherData
    });  // trigger events
      res.json(project)
  }catch(e){
    return res.status(400).json({msg: e.message})
  }
})


router.post("/:slug/faq", auth,async(req, res)=>{
  // const {content, projectID, userID} = req.body
  try{
    const { error } = validateProjectFaq(req.body); 
    if (error) return res.status(400).json({msg: error.details[0].message});
    let faq = new ProjectFaq({
      question:req.body.question,
      answer:req.body.answer,
      project:req.body.project
    })

    await faq.save()
    let project = await Project.findOneAndUpdate({slug:req.params.slug},
      { $push: { faq: faq._id } },
      { new: true, useFindAndModify: false }
      )
      // ==========================NOTIFY THE OWNER OF THE CAMPAIGN THAT HE HAS A NEW COMMENT ==========
    // const notifData = new Notification({
    //   ownerID: project.userID,
    //   message: `
    //     Your Campaign, ${project.project_name} Just Received A New Comment
    //     `,
    //     link : `/campaigns/${req.params.slug}`
    // });

    // // save the notification
    // await notifData.save()
      
      res.json(project)
  }catch(e){
    return res.status(400).json({msg: e.message})
  }
})

router.put("/:slug/like", auth,async(req, res) => {
    try {
      const project = await Project.findOneAndUpdate({slug:req.params.slug}, {
        $push:{
          like:req.user.id
        }
      },{new: true}) .populate("updates")
      .populate("userID")
      .populate("category")
      .populate("faq")
      .populate("like")
      .populate("project_comments")
      .populate({
        path:"project_comments",
        populate:{path:"owner"}
      })
      const notifData = new Notification({
        ownerID: project.userID,
        message: `
          Your Campaign '${project.project_name}' was liked 
          `,
        // link: `/dashboard/campaign/${project.slug}`,
      });
      await notifData.save();
        pusher.trigger('Elesarr', 'LIKED', {

          message: 'success',
          // project_name: project_name,
        });
      res.json(project)
    }catch(e){
      return res.status(400).json({msg: e.message}) 
    }
})


// 5f032eb945611c0017d43465

// USER ID 5ef4cad5d503a90017133db1

/** END ADD UPDATE TO CAMPAIGN */
/** UPDATE THIS ROUTE USING SLUG */

// @route   delete api/users
// @desc    delete a user
// @access  Public
router.delete("/delete/:slug", auth,(req, res) => {
  
  Project.findOne({slug:req.params.slug})
    .then(project =>{
        if(project.userID == req.user.id){

         
        const notif = new Notification({
          ownerID:req.user.id,
          message: `You successfully deleted your campaign named ${project.slug} `,
          link: `/dashboard`,
        });
        notif.save().catch((err) => {
          // console.log("failed to save notification", err.message);
          return res.status(400).json({ msg: err.message});
        });
        const pusherData = {
          ownerID: req.user.id,
        };

          project.remove().then(() => res.json({ message:"Deleted Successfully" }))
        }  else{
         return res.status(403).json({msg:"NOT AUTHORIZED TO DELETE PROJECT "})
        }
    })
    .catch(err => res.status(404).json({ msg: err.message }));
});

router.get("/categories",async (req, res) => {
  try{
    const categories = await Category.find()
    res.json(categories)
  }catch(e){
    return res.status(400).json({msg:e.message})
  }

})
/** REAL CONTRIBUTROS ROUTES */
router.get("/:projectID/contributors", async(req, res)=>{
  try{
    const contributors  = await   Guest.find().where('projectID').equals(req.params.projectID)
    res.json(contributors) 
  }catch(err){
    return res.status(400).json({msg:err.message})
  }
})

/** END  REAL CONTRIBUTORS ROUTES */
/** CONTRIBUTOR LENGTH */

router.get("/:projectID/contributors/length",async(req, res)=>{
  try{
    const contributors = await   Guest.find().where('projectID').equals(req.params.projectID)
    res.json(contributors.length)
  
  }catch(err){
    return res.status(400).json({msg:err.message})
  }

})

router.get("/contributors/length", async(req,res) => {
  try{
    const contributors = await Guest.find()
    res.json(contributors.length)
  }catch(e){
    return res.status(400).json({msg:e.message})
  }  
})

router.get("/totalBalance", async(req, res)=>{
  try{
      const getBalance = await Project.aggregate([{
          $group: {
               _id: null,
               count: { $sum: "$project_current_balance" }
           }
       }
       ])
       if(getBalance.length != 0){
        res.json(getBalance)
       } else{
         res.json(0)
       }
  }catch(e){
      return res.status(400).json({msg:e.message})
  }
  })
  
/** END CONTRIBUTOR LENGTH */

router.get("/length", async(req, res) => {
  try{
    const projects = await Project.find()
                                  .where("status")
                                  .equals("publish")
    res.json(projects.length)
  }catch(e){
    return res.status(400).json({msg:e.message})
  }
})

router.put("/:id/edit-category", async(req, res) => {
  try{
    const category = await Category.findByIdAndUpdate(req.params.id, {
      $set:{
        "category_name":req.body.category_name
      }
    })
    res.json(category)
  }catch(e){
    return res.status(400).json({msg:e.message})

  }
})
module.exports = router;