require('dotenv').config()
const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");


// import multer and the AvatarStorage engine
// var _ = require('lodash');
// var path = require('path');
// var multer = require('multer');
// var AvatarStorage = require('../../helpers/AvatarStorage');
// var Profile = require('../../model/Profile')

// User Model
// const User = require("../models/SignupUser");
const User = require("../../model/OrganisationSignup")
// @route   POST api/users
// @desc    Register new user
// @access  Public

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);
const api_Key = '5779602fb87b8fb328ed684d05ea7339-7fba8a4e-5517562d'
const domain = 'elesarr.com';
var mailgun = require('mailgun-js')({apiKey: api_Key, domain: domain});


router.post("/", (req, res) => {
  const { 
    name_of_organisation, 
    email, 
    telephone_no,
    office_address,
    rc_no,
    location_of_company,
    country,
    state,
    postal_code,
    password,
    confirmPassword} = req.body;

  // Simple validation
  if (
    !name_of_organisation || 
    !email || 
    !telephone_no || 
    !office_address || 
    !rc_no || 
    !location_of_company || 
    !country || 
    !state || 
    !postal_code || 
    !password || !confirmPassword) {
    return res.status(400).json({ msg: "Please enter all fields" });
  }
  if(password !== confirmPassword){
    return res.status(400).json({ msg: "Password Must Match" });
  }
  // Check for existing user
  User.findOne({ email }).then(user => {
    if (user) return res.status(400).json({ msg: "User already exists" });
    const newUser = new User({
        name_of_organisation, 
        email, 
        telephone_no,
        office_address,
        rc_no,
        location_of_company,
        country,
        state,
        postal_code,
        password
    });

    // Create salt & hash
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(newUser.password, salt, (err, hash) => {
        if (err) throw err;
        newUser.password = hash;
        newUser.save().then(user => {
          jwt.sign(
            { id: user.id },
            process.env.jwtSECRET,
            { expiresIn: 20000 },
            (err, token) => {
              if (err) throw err;

            const msg = {
              to: `${newUser.email}`,
              from: 'goodness@elesarr.com',
              subject: 'THANKS FOR SIGING UP',
              // text: 'and easy to do anywhere, even with Node.js',
              html:`<h5>Thanks For Signing Up On Our Platform!!!. </h1>
              <p>Hi, my name is Goodness and i want to appreciate you for signing up on Elesarr</p>
              <p>Click <a href="https://elesarr.com/login">here</a> to start a campaign</p>
              <p>If you encounter any issues you can email customercare@elesarr.com and our team will respond to you in no time</p>
              <p>THANK YOU!!!</p>
            `,
            };
            sgMail.send(msg);

              res.json({
                token,
                // user: {
                  id: user.id,
                  name_of_organisation: user.name_of_organisation,
                  email: user.email,
                  class:user.class,
                  date : user.timestamp,
                  country:user.country,
                  verified : user.verified
                // }
              });
            }
          ).catch(err =>console.log(err));
        });
      });
    });
  });
});

// @route   POST api/users
// @desc    list all user
// @access  Public

router.get("/all", (req, res) => {
  User.find()
    // .sort({date : -1})
    .then(users => res.json(users));
});



// @route   PUT api/users
// @desc    update a user
// @access  Public


router.put("/user/:id", (req, res) => {
  var id = req.params.id;

  const {
      name_of_organisation, 
      email, 
      telephone_no, 
      office_address,
      rc_no, 
      location_of_company, 
      country, 
      state, 
      postal_code } = req.body
  console.log(id)


  
  User.findByIdAndUpdate(id, {
    $set: {
      name_of_organisation: name_of_organisation,
      email: email,
      telephone_no : telephone_no,
      office_address : office_address,
     rc_no : rc_no,
      location_of_company : location_of_company,
      country : country,
      state : state,
      postal_code : postal_code

    }
  })
    .then(() =>  User.findById(id)

    ).then(user => res.json({updated : user}))
    .catch(err => res.status(204).json({ success: err }));
});


// @route   delete api/users
// @desc    delete a user
// @access  Public
router.delete('/delete/:id', (req, res) => {
  User.findById(req.params.id)
    .then(user => user.remove().then(() => res.json({ success: true })))
    .catch(err => res.status(404).json({ success: false }));
});

// @route   delete api/users
// @desc    get a user
// @access  Public
router.get('/user/:id', (req, res) => {
  User.findById(req.params.id)
    .then(user => res.json(user))
    .catch(err => res.status(404).json({ failed: err }));
});
// start here












module.exports = router;
