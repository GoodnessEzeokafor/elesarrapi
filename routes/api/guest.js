/** IMPORT NODE MODULES */
const express = require("express");
const router = express.Router();
const _ = require("lodash");
const request = require("request");
const { initializePayment, verifyPayment } = require("../../config/paystack")(
  request
);
const { Convert } = require("easy-currencies");
/**END IMPORT */
var async = require('async');
const api_Key = process.env.MAILGUNKEY;
const domain = "elesarr.com";
var mailgun = require("mailgun-js")({ apiKey: api_Key, domain: domain });

/** IMPORT MODELS */
const Pay = require("../../model/Pay");
const {Project} = require("../../model/Project");
const {Notification} = require("../../model/Notification");
/** PUSHER JS CONFIGURATION */
const Badge = require("../../model/Badge")
var Pusher = require("pusher");
var pusher = new Pusher({
  appId: '1004110',
  key: '2347f2ea036eda8b245e',
  secret: 'b072af717d87f7f792a5',
  cluster: 'eu',
  encrypted: true
});
const {Guest,validateGuest} = require("../../model/Guest")

/**  PUSHER JS CONFIGURATION */

/** END IMPORTS */

//================================paystack post routes=============================
router.post("/:slug/pay", (req, res) => {
  if (!req.body.amount || !req.body.email || !req.body.full_name) {
    return res.status(400).json({ msg: "Please enter all the data required" });
  }
  // console.log("request params being sent to /pay", req)
  const form = _.pick(req.body, ["amount", "email", "full_name"]);
  console.log(form);

  form.metadata = {
    full_name: form.full_name,
    email: form.email,
    projectID: req.params.slug,
  };
  form.amount *= 100;
  // console.log("userId", form.metadata.userID)
  initializePayment(form, (error, body) => {
    if (error) {
      //handle errors
      console.log("error message ", error);
      // return res.redirect("/error");
      res.json({ response: response.data });
      return;
    }
    response = JSON.parse(body);

    // res.redirect(response.data.authorization_url);
    res.json({ response: response.data.authorization_url });
  });
});

/** Flutterwave CALLBACK */

router.post("/callback", (req, res) => {
  const {reference, amount,email,full_name, projectID} = req.body
  console.log(
    "details i need : ",
    full_name,
    projectID,
    reference,
    amount,
    email
  );




  newPay = { reference, amount, email, full_name, projectID };
  console.log("new pay", newPay);

  const pay = new Guest(newPay);

  // Trying to save the transaction to the user tables
  const date = Date.now;
  // const amounts = amount ;
  pay.save().then((pay) => {
    if (!pay) {
      return res.redirect("/error");
    }
    Project.findOneAndUpdate({slug:projectID}, {
      $inc: {
        project_current_balance: amount
      },
    })
      .then((data, err) => {
        if(err) return res.status(400).json({ msg: err.message});
        console.log("updated")
      const msg = {
        to: `${email}`,
        from: 'Emmanuel From Elesarr <Emmanuel@elesarr.com>',
        subject: 'APPRECIATION',
        // text: 'and easy to do anywhere, even with Node.js',
        html:`<h5>Thanks For Your Donation ${full_name}. </h1>
        <p>Thank you ${full_name} for donating on elesarr.com.We are thrilled to have your support</p>

        <p>You have been rewarded with a backers badge on our platform, signup with the email you used to make
        payment to claim your badge, if you are already part of our community simply login to claim your reward.You can also tweet about your reward and tag us(@elesarrO)
        </p>


        <p>
          Kindly reply with your Facebook handle or Twitter handle, we would love to give you a 
          shout out on our various social media platforms.
          You can join our whatsapp and telegram community
        </p>
        <p>
          Whatsapp community click <a href="https://chat.whatsapp.com/KMMCDyJKzheHcp2brMun0E" >here</a> <br />
          Telegram community click <a href="https://t.me/joinchat/MgHjcBilXKj1W1p0qi9Z8A">here</a> 
        </p>
        <p>THANK YOU!!!</p>
      `,
      };
      /** SEND MAIL */              
      mailgun.messages().send(msg, function (error, body) {
        if (error){
          return res.status(400).json({ msg: error.message});
        }
        console.log("EMAIL SENT!!!");
      });
      /** SEND MAIL */
      
      /** REWARD MEMBERSHIP BADGE */
      const badge = new Badge({
        user_email:email,
        badgeurl:"https://res.cloudinary.com/elesarr/image/upload/v1593793061/badges/ElesarrBacker_uqnegz.png",
        status:"backer"
      }) // create badge 
      badge.save()
      /** REWARD MEMBERSHIP BADGE */

      // console.log("owner id ", data.userID._id)
      
        // const notif = new Notification({
        //   ownerID: data.userID._id,
        //   message: `Your Campaign Received A Donation Of ${amount} from ${email} `,
        //   link: `/dashboard/${data.slug}/project`,
        // });
        // notif.save().catch((err) => {
        //   // console.log("failed to save notification", err.message);
        //   return res.status(400).json({ msg: err.message});
        // });
        // const pusherData = {
        //   ownerID: data.userID,
        // };
        // // PUSHER TO TRIGGER A NOTIFICATION TO THE OWNER OF THE PROJECT
        // pusher.trigger("Elesarr", "PROJECT_FUNDED", {
        //   pusherData,
        // }); // trigger events

return res.json("Success")
        // return res.redirect("https://elesarr.herokuapp.com/thanks")

      })
      .catch((err) => {
        return res.status(400).json({ msg: err.message});

      });
    // NOTIFICATION ENDS

    // Project.findOneAndUpdate({slug:projectID}, {
    //   $inc: {
    //     project_current_balance: pay.amount
    //   },
    // })

    //   // .then(() => res.json({ success: true }))
    //   .then(() => {
    //     return res.json("success")
    //   })      
    //   .catch((err) =>{
    //     return res.status(400).json({ msg: err.message});
    //   } 
    //   );

  });



  // console.log(ref)
  // console.log("Callback stagee");
  // verifyPayment(ref, (error, body) => {


  //   if (error) {
  //     console.log(error);
  //     return res.json({ msg: error });
  //   }
  //   response = JSON.parse(body);
  //   console.log("Verify payment response", response.data);

  //   const { reference, amount } = response.data;
  //   const { email } = response.data.customer;

  //   const { full_name, projectID } = response.data.metadata;

  //   console.log(
  //     "details i need : ",
  //     full_name,
  //     projectID,
  //     reference,
  //     amount,
  //     email
  //   );

  //   newPay = { reference, amount, email, full_name, projectID };
  //   console.log("new pay", newPay);

  //   const pay = new Guest(newPay);

  //   // Trying to save the transaction to the user tables
  //   const date = Date.now;
  //   const amounts = amount / 100;
  //   pay.save().then((pay) => {
  //     if (!pay) {
  //       return res.redirect("/error");

  //       //   res.json("Save Failed");
  //       //   console.log('save failed')
  //     }
  //     console.log("payment owner method ", pay.method);

  //     // NOTIFICATION TO THE OWNER OF THE PROJECT WHEN A GUEST PAYS

  //     Project.findById(projectID)
  //       .then((data) => {
  //         console.log("data ", data);
  //         const notif = new Notification({
  //           ownerID: data.userID,
  //           message: `Your Campaign Received A Donation Of ${amounts} from ${email} `,
  //           link: `/dashboard/${data.slug}/project`,
  //         });

  //         notif.save().catch((err) => {
  //           console.log("failed to save notification", err);
  //         });

  //         const pusherData = {
  //           ownerID: data.userID,
  //         };
  //         // PUSHER TO TRIGGER A NOTIFICATION TO THE OWNER OF THE PROJECT
  //         pusher.trigger("Elesarr", "PROJECT_FUNDED", {
  //           pusherData,
  //         }); // trigger events
  //       })
  //       .catch((err) => {
  //         console.log("failed to save notification", err);
  //       });
  //     // NOTIFICATION ENDS

  //     Project.findByIdAndUpdate(projectID, {
  //       $inc: {
  //         project_current_balance: pay.amount / 100,
  //       },
  //     })

  //       // .then(() => res.json({ success: true }))
  //       .then(() => console.log("sucessful"))
  //       .then(() => res.redirect("https://elesarr.com/thanks"))
  //       .catch((err) => res.status(204).json({ success: err }));

  //     // console.log("normal pay id ", pay._id);
  //     // // res.redirect("/api/paystack/receipt/" + pay._id);
  //     // res.redirect("http://elesarr.herokuapp.com/");
  //   });


  // });


});




/**  END flutterwave CALLBACK */



// test 

router.post("/convert", (req,res)=>{
 
  Convert(1)
  .from("USD")
  .to("NGN").then(data =>{
    return res.json(data); // converted value
  }).catch(err=>{
    console.log(err)
  });
})



module.exports = router;
// 5e9c2e325b2d3e001791b9df
