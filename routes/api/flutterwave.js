require('dotenv').config()
const express = require('express');
const router = express.Router();
const request= require("request")
const Ravepay = require('flutterwave-node');
const rave = new Ravepay("FLWPUBK_TEST-f3112ebed784f77d79651d7144589500-X", "FLWSECK_TEST-29bf6f3dab58af47e1e005b6ec6320c0-X", false); //Base url is 'http://api.ravepay.co'


const url = require('url');    


router.post("/pay",(req, res)=>{
    // console.log("FLUTTERWAVE")
    let ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
    const{
        cardno,
        cvv,
        month,
        year,
        currency,
        country,
        amount,
        email,
        // phonenumber,
        firstname,
        lastname
    }=req.body
    rave.Card.charge(
        {
            // "4187427415564246"
            // "564"
            // "564"
            // 09
            //"21"
            //"NGN"
            //"NG"
            //"NG"
            //"user@gmail.com"
            //"09018916522"
            //"temi"
            //"desola"

            "cardno":cardno ,
            "cvv": cvv,
            "expirymonth": month,
            "expiryyear": year,
            "currency": currency,
            "country": country,
            "amount": amount,
            "email": email,
            // "phonenumber": phonenumber,
            "firstname": firstname,
            "lastname": lastname,
            "IP": ip,
            "txRef": "MC-" + Date.now(),// your unique merchant reference
            "meta": [{metaname: "flightID", metavalue: "123949494DC"}],
            "redirect_url": "https://rave-webhook.herokuapp.com/receivepayment",
          }
    ).then(resp => {
        // console.log(resp.body);
        // res.redirect(resp.body.authurl)
        return res.json({
            pathname:"http://localhost:3000/flutterwave/callback",
            'resp':resp.body.data.flwRef            
        }) 

        // res.redirect(
        //     url.format({
        //         pathname:"http://localhost:5200/flutterwave/callback",
        //         query:{
        //             'resp':resp.body.data.flwRef
        //         }
        //     }))
            // `http://localhost:5200/api/flutterwave/callback/?resp=${resp.body.data.flwRef}`)
        // .status(200)
        // .json({
        //   msg: "Paypment Pending, Enter OTP CODE",
        //   url: `http://localhost:3000/api/flutterwave/callback/?resp=${resp.body.data.flwRef}`,
          
        // });
        // res.redirect("")
        
    }).catch(err => {
        console.log(err);
        
    })
})

router.post("/callback", (req, res)=>{
    console.log("FLUTTER WAVE CALLBACK")
    // console.log(res)
    const {otp, resp}= req.body
rave.Card.validate({
    "transaction_reference":resp,
    "otp":otp
}).then(response => {
    console.log(response.body.data);
    res.json({
        success:"Payment Made"
    })  
})    
})



module.exports = router;



