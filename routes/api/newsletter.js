const express = require("express");
const router = express.Router();
const {Newsletter,validateNewsletter} = require("../../model/Newsletter")
const request= require("request")
router.post("/", async(req, res) => {
    try{
        const { error } = validateNewsletter(req.body); 
        if (error) return res.status(400).json({msg: error.details[0].message})
        let newsletter = new Newsletter({email:req.body.email})
        await newsletter.save() // save newsletter
            /** MAILCHIMP */
    const data = {
        members:[
          {
            email_address:req.body.email,
            status:"subscribed",
            // merge_fields:{
            //     FNAME:username,
            // }
          }
        ]
      }
      const postData = JSON.stringify(data) 
      const options = {
        url :"https://us19.api.mailchimp.com/3.0/lists/02e1d16e87",
        method:'POST',
        headers:{
          Authorization:"auth e56abf2dd98943505e8fcaee5e36ea02-us19"
        },
        body:postData
      };
  
      request(options, (err, response,body)=>{
        if(err){
          console.log("MAILCHIMP: ERROR", err)
        } else{
          if(response.statusCode === 200){
            console.log("SUCCESS")
          } else {
            console.log("FAILED")
          }
        }
      })
      
      /** END MAILCHIMP */
        res.json(newsletter)
    }catch(e){
        return res.status(400).json({msg:e.message})
    }
})

router.get("/length", async(req, res) => {
        try{
            const newsletters = await Newsletter.find()
            res.json(newsletters.length)
        }catch(e){
            return res.status(400).json({msg:e.message})
        }
})

module.exports = router;