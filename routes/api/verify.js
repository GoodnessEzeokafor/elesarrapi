require('dotenv').config()
const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const fs = require('fs');

/*
  ROUTE FOR NORMAl USERS
*/

// import multer and the AvatarStorage engine
var _ = require('lodash');
var path = require('path');
var multer = require('multer');
const auth = require("../../middleware/auth")
// var Profile = require('../../model/Profile')
// User Model
// const User = require("../models/SignupUser");
const {User} = require("../../model/SignupUser")

// const nodemailer = require("nodemailer");

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);



// var transporter = nodemailer.createTransport({
//   // service: 'gmail',
//   host: 'elesarr.com',
//   port: 465,
//   auth: {
//     user: 'customercare@elesarr.com',
//     pass: process.env.PASS
//   }
// });








// @route   post api/verify/apply
// @desc    apply to be verified
// @access  Public
router.post('/apply', (req, res) => {


    const {email, socialMedia,userID,phoneNumber } =req.body

    if(!email || !socialMedia || !userID ||phoneNumber){
        console.log("please enter all fields")
        return res.status(400).json("Please Enter All Fields")
    }



    const msg = {
        to: `emmanuelsumeh@gmail.com`,
        from: 'customercare@elesarr.com',
        subject: 'Verefication Request',
        // text: 'and easy to do anywhere, even with Node.js',
        html:`<h1>New Verification request!!!. </h1>
        <p>A user with the email ${email} and id of ${userID} sent a request to be verified</p>
        <p>Users social media handle is <a href="${socialMedia}">here</a> and phone Number is ${phoneNumber}</p>
        <p>Click <a href="http://localhost:3000/verify/?userID=${userID}&email=${email}&phoneNumber=${phoneNumber}">here</a> to verify the user</p>
      
        <p>THANK YOU!!!</p>
      `,
      };
      sgMail.send(msg).then(
          res => {
              console.log(res)
              res.status(200).json("Your Request to Confirm Your Profile Has Been Sent, It Will Be Reviewed and FeedBack WIll Be Received Shortly")
          }
      )

});


// @route   post api/verify/verify
// @desc    user to be verified
// @access  Public

router.post('/verify/', (req, res) => {


    const {userID , email, phoneNumber,apiKey} =req.body

    const apiKeyDefault = process.env.apiKey


console.log("req.body", req.body)
    console.log(userID, email,phoneNumber,apiKey)

    console.log(apiKeyDefault)

    if(apiKey !== apiKeyDefault || !apiKey){
        return res.status(400).json("api key is incorrect")
    }

if(!userID || !email ||!phoneNumber){
    console.log("Some OF the Required user data is missing")
    return res.status(400).json("Some OF the Required user data is missing")

}



    User.findById(userID).then(
        user => {
           
                console.log("searching  individual")
                User.findByIdAndUpdate(userID, {
                    $set : {
                        verified : true,
                        "profile.telephone" : phoneNumber
                    }
                })
                // .then(res.status(200).json({msg : "user was verified succesfully"}))
                .then(res.json("User was verified successfully"))
           
           
        }
    )
 


    const msg = {
        to: `${email}`,
        from: 'customercare@elesarr.com',
        subject: 'Verification Successful',
        // text: 'and easy to do anywhere, even with Node.js',
        html:`<h1>Congratulations!!!. </h1>
        <p>You sent a request from your account on <a href="https://elesarr.com">elesarr</a> to be verified </p>
        <p>Your request has beeen confirmed and you are now verified on elesarr.com</p>
        <p>Click <a href="https://elesarr.com">here</a> to create or back a campaign</p>
      
        <p>THANK YOU!!!</p>
      `,
      };
      sgMail.send(msg);



});









module.exports = router;
