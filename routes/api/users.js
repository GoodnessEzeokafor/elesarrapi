require('dotenv').config()
const express = require("express")
const router = express.Router();
const nodemailer = require("nodemailer");
const request= require("request")
const User = require("../../model/user")

var transporter = nodemailer.createTransport({
    // service: 'gmail',
    host: 'elesarr.com',
    port: 465,
    auth: {
      user: 'customercare@elesarr.com',
      pass: process.env.PASS
    }
  });


  

// @route   POST api/users
// @desc    Register new user
// @access  Public
router.post("/", (req, res) => {
    // if(req.method !== "local" ){
    //   next()
    // }
  
    const {email} = req.body;
    // console.log(email)
  
    // Simple validation
    if (!email) {
      return res.status(400).json({ msg: "Please enter your email" });
    }
  
    // Check for existing user
    User.findOne({ "user.email" : email }).then(user => {
        console.log("line 37", user)
  
      if (user) return res.status(400).json({ msg: "User already exists" });
  
      const newUser = new User({
       
        user :{
          email
          
        }

        
      });
      var mailOptions = {
        from: 'customercare@elesarr.com',
        to:    `${newUser.user.email}`,
        subject: 'Thank You For Signing Up',
        text: 'You have successfully signed up with elesar. We will notify you when the project get launched!!!'
      };

      console.log("new user ", newUser.user.email)
      const data = {
        members:[
          {
            email_address:newUser.user.email,
            status:"subscribed"
          }
        ]
      }
      const postData = JSON.stringify(data) 
      const options = {
        url :"https://us19.api.mailchimp.com/3.0/lists/02e1d16e87",
        method:'POST',
        headers:{
          Authorization:"auth e56abf2dd98943505e8fcaee5e36ea02-us19"
        },
        body:postData
      };

      request(options, (err, response,body)=>{
        if(err){
          console.log("MAILCHIMP: ERROR", err)
        } else{
          if(response.statusCode === 200){
            console.log("SUCCESS")
          } else {
            console.log("FAILED")
          }
        }
      })
newUser.save().then( user => {
       
          transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
            } else {
              console.log('Email sent: ' + info.response);
              res.send({message:"Successfully Sent and Saved!!"})
            }
          });
    }
).catch(err => console.log(err))
// .then(
//     res => {
//         res.status(200).json({msg : "registered successfully"})
//     }
// )
     
    });
  });

  // @route   GET api/users
// @desc    list all user
// @access  Public

router.get("/", (req, res) => {
    User.find()
      // .sort({date : -1})
      .then(users => {
        // console.log(process.env.PASS)
        res.json(users)});
  });
  
module.exports = router;

// MONGO_URL='mongodb+srv://ElessarAdmin:r8N1xFAUKridhcTN@cluster0-8gelz.mongodb.net/test?retryWrites=true&w=majority'
// PASS='[hlc~~L1LF$C'
