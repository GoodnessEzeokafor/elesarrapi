require('dotenv').config()
const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');

const jwt = require('jsonwebtoken');


/** import models */
const {User}  = require('../../model/SignupUser');
const {Category, validateCategry} = require("../../model/Category")
const {Project} = require("../../model/Project")
const {Newsletter,validateNewsletter} = require("../../model/Newsletter")
const {BlogCategory,validateBlogCategry} = require("../../model/BlogCategory")
const {Post}  = require('../../model/Post');

/** import models */



/** AUTH & ADMIN */
const auth = require("../../middleware/auth")
const staff = require("../../middleware/staff")
const admin = require("../../middleware/admin")

const _ = require("lodash")
/** AUTH & ADMIN */


/**STAFF: SET STAFF */
router.put("/set-staff/:id",auth,admin,async(req, res)=> {
    try{
        const user = await User.findByIdAndUpdate(req.params.id, {
            $set:{
                "isStaff":true
            }
        })
        res.json(user)
    }catch(e){
        return res.status(400).json({msg:err.message})
    }
})

/**STAFF:  PUBLISH PROJECT */
router.put("/get-project/:slug/publish",auth,staff,async(req, res)=> {
    try{
        const project = await Project.findOneAndUpdate({slug:req.params.slug}, {
            $set:{
                "status":"publish"
            }
        })
        res.json(project)
    }catch(e){
        return res.status(400).json({msg:err.message})
    }
})

router.put("/get-posts/:slug/publish",auth,staff,async(req, res)=> {
    try{
        const post = await Post.findOneAndUpdate({slug:req.params.slug}, {
            $set:{
                "status":"publish"
            }
        })
        res.json(post)
    }catch(e){
        return res.status(400).json({msg:err.message})
    }
})


router.put("/get-projects", auth, staff, async(req, res) => {
    try{
            const projects = await Project.find()
            res.json(projects)
    }catch(e){
        return res.status(400).json({msg:err.message})

    }
})

router.put("/get-projects/draft",auth, staff, async(req, res) => {
    try{
        const projects = await Project.find()
                                .where("status")
                                .equals("draft")

        res.json(projects)
    }catch(e){
        return res.status(400).json({msg:err.message})
    }
})

router.put("/get-posts/draft",auth, staff, async(req, res) => {
    try{
        const posts = await Post.find()
                                .where("status")
                                .equals("draft")

        res.json(posts)
    }catch(e){
        return res.status(400).json({msg:err.message})
    }
})

module.exports = router;