require("dotenv").config();
const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");

const jwt = require("jsonwebtoken");

// User Model
const {User} = require("../../model/SignupUser");

const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const api_Key = process.env.MAILGUNKEY
const domain = 'elesarr.com';
var mailgun = require('mailgun-js')({apiKey: api_Key, domain: domain});

// var transporter = nodemailer.createTransport({
//   // service: 'gmail',
//   host: 'elesarr.com',
//   port: 465,
//   auth: {
//     user: 'customercare@elesarr.com',
//     pass: process.env.PASS
//   }
// });

// Log in a user

// @route   POST api/auth
// @desc    Auth user
// @access  Public

router.post("/", (req, res) => {
  const { email, password } = req.body;

  // console.log(email, password)
  console.log("PASSWORD", password);
  // Simple validation
  if (!email || !password) {
    return res.status(400).json({ msg: "Please enter all fields" });
  }

  // Check for existing user
  User.findOne({ email: email })
    .then((user) => {
      
      // Validate password
      // console.log("user password", user.local.password)

      bcrypt.compare(password, user.password).then((isMatch) => {
        if (!isMatch)
          return res.status(400).json({ msg: "Invalid credentials" });

        console.log(user._id);

        jwt.sign(
          { id: user._id , isAdmin:user.isAdmin, isWriter:user.isWriter, isStaff:user.isStaff},
          process.env.jwtSECRET,
          { expiresIn: 10000 },
          (err, token) => {
            if (err) throw err;
            res.status(200).json({
              token,
              // user: {
              email: user.email,
              username: user.usernamme,
              profile: user.profile,
              class: user.class,
              id: user._id,
              isAdmin: user.isAdmin,
              isStaff: user.isStaff,
              verified: user.verified,
              isWriter:user.isWriter,
              msg: "logged in successfully",
              
            });
          }
        );
      });
    })
    .catch((e) => {
      // res.redirect("/");
      return res.status(400).json({ msg: "Invalid credentials" });
    });
});

// this is to ensure the user email is correct
var confirm;

router.post("/forgotPassword", (req, res) => {
  const { email } = req.body;

  User.findOne({ email: email })
    .select("-password")
    .then((user) => {
        // if its an individual
        function makeid(length) {
          var result = "";
          var characters =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
          var charactersLength = characters.length;
          for (var i = 0; i < length; i++) {
            result += characters.charAt(
              Math.floor(Math.random() * charactersLength)
            );
          }
          return result;
        }

        confirm = makeid(6);
        const msg = {
          to: `${email}`,
          from: "customercare@elesarr.com",
          subject: "Password Reset",
          // text: 'and easy to do anywhere, even with Node.js',
          html: `<h1>Hi ${user.username} </h1>
        <p>We received a request to reset the password on your Elesarr Account. </p>
        <h1>${confirm}</h1>
        <p>Enter this code to complete the reset.</p>
      
        <p>Thanks for helping us keep your account secure.</p>
        <p>The Elesarr Team.</p>
      `,
        };
        mailgun.messages().send(msg, function (error, body) {
          if (error){
            console.log(error);
          }
          //  console.log("EMAIL SENT!!!");
          //  res.status(200).json("password reset successful");
          return res
              .status(200)
              .json({
                msg: "password reset request sent successful",
                url: ` https://elesarr.com/confirmEmail/?id=${user._id}`,
                code: confirm,
                id: user.id,
              });
        });


      

    });
});

// this is to change the actual password
router.post("/resetPassword", (req, res) => {
  var { id, password, confirmPassword } = req.body;

  if (password !== confirmPassword) {
    return res.json("Passwords do not match");
  }

  User.findById(id)
    // .select("-password")
    .then((user) => {


        console.log("initial password ", password);
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(password, salt, (err, hash) => {
            if (err) throw err;
            password = hash;

            console.log("password has been hashed ", password);
            User.findByIdAndUpdate(id, {
              $set: {
                password: password,
              },
            }).then(
              User.findById(id).then((user) => {
                const msg = {
                  to: `${user.email}`,
                  from: "customercare@elesarr.com",
                  subject: "Password Reset",
                  // text: 'and easy to do anywhere, even with Node.js',
                  html: `<h1>Hi ${user.username} </h1>
                <p>Your Password Has Been Reset Successfully</p>
                <p>You can now login at <a href = "elesarr.com/login">Elesarr </a> </p>
              
              
                <p>Thanks for helping us keep your account secure.</p>
                <p>The Elesarr Team.</p>
              `,
                };

                mailgun.messages().send(msg, function (error, body) {
                  if (error){
                    console.log(error);
                  }
                   console.log("EMAIL SENT!!!");
                   res.status(200).json("password reset successful");
                });

              })
            );
          });
        });
      

    });
});

module.exports = router;
