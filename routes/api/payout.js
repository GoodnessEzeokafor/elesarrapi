require('dotenv').config()
const express = require('express');
const router = express.Router();
const {Payout,validatePayout} = require("../../model/Payout")
const {Project} = require("../../model/Project")
const Pay = require("../../model/Pay")
const Guest = require("../../model/Guest")
const nodemailer = require("nodemailer");
const auth = require("../../middleware/auth")
const {Notification} = require("../../model/Notification")
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const api_Key = '5779602fb87b8fb328ed684d05ea7339-7fba8a4e-5517562d'
const domain = 'elesarr.com';
var mailgun = require('mailgun-js')({apiKey: api_Key, domain: domain});

/** PUSHER JS CONFIGURATION */
var Pusher = require("pusher");
var pusher = new Pusher({
  appId: '1004110',
  key: '2347f2ea036eda8b245e',
  secret: 'b072af717d87f7f792a5',
  cluster: 'eu',
  encrypted: true
});

/**  PUSHER JS CONFIGURATION */








router.post("/:slug",auth, async (req, res)=> {

  const { error } = validatePayout(req.body); 
  if (error) return res.status(400).json({msg: error.details[0].message})

    // const {email, amount, userID, projectID, accountNO, bankName,bankCode} = req.body

    try{
        const project = await Project.findOne({slug:req.params.slug})
        const currentDate = new Date()
        if(req.user.id != project.userID)  return res.status(403).json({msg: "NOT AUTHORIZED TO MAKE WITHDRAWALS"})

        const msg = {
          to: `${req.body.email}`,
          from: 'goodness@elesarr.com',
          subject: 'Goodness From Elesarr',
          // text: 'and easy to do anywhere, even with Node.js',
          html:`
            <h6>Thanks Your Request Has Been Made</h6>
            <p>Payment will be processed in three business days</p>
            <p>Thanks For Using Our Platform! </p>
        `,
        };
        await sgMail.send(msg);
        const newPayout= new Payout({
                                  email:req.body.email, 
                                  amount:project.project_current_balance, 
                                  userID:req.user.id,
                                  projectID:req.body.projectID, 
                                  accountNO:req.body.accountNO,
                                  bankName:req.body.bankName
                              })
      await newPayout.save() // save the payout

      const notif = new Notification({

        ownerID : project.userID,
        message : `Your Request to Cashout Was Made Successfully`,
        link : `/dashboard/${project.slug}/project`
      })
      await notif.save()
      const pusherData ={
        ownerID: project.userID
      }
      // PUSHER TO TRIGGER A NOTIFICATION TO THE OWNER OF THE PROJECT
      pusher.trigger("Elesarr", "CASHOUT_REQUEST", {
        pusherData,
      });  // trigger events

    }catch(e){
      return res.status(400).json({msg:e.message})
    }

})


router.get("/", auth, async(req, res)=> {
  try{
    const payouts = await Payout.find()
    res.json(payouts)
  }catch(e){
    return res.status(400).json({msg:e.message})
  }
})
router.get("/length",auth,async(req, res)=> {
  try{
    const payouts = await  Payout.find()
    res.json(payouts)
  }catch(e){
    return res.status(400).json({msg:e.message})
  }

})



router.get("/guests", (req, res)=>{
  Guest.find()
      .then(guest=>{
        if(!guest){
          res.json({
            message:"NO GUEST BACKERS"
          })  
        }
        return res.json(guest)
      })
})
router.get("/", (req,res)=>{
  Payout.find()
    // .sort({date : -1})
    .then(payout =>{
      if(!payout){
        res.json({
          message:"NO PROJECTS CREATED YET"
        })
      } 
      return res.json(payout)   
  })
})






module.exports = router;


// {"_id":
//   {"$oid":"5e9b6038b14261001721d4eb"},
//   "project_current_balance":{"$numberInt":"2500"},
//   "completedAt":{"$date":{"$numberLong":"0"}},
//   "like":"0",
//   "useCrypto":false,
//   "flag":false,
//   "userID":"5e9b3faa87857414dcd1eb40",
//   "project_name":"Personal Project",
//   "project_description":
//   "Personal Project Description",
//   "image_slide_1":"https://res.cloudinary.com/elesarr/image/upload/v1587235994/campaign/individual/slide1/5e9b3faa87857414dcd1eb40.jpg","image_slide_2":"https://res.cloudinary.com/elesarr/image/upload/v1587235994/campaign/individual/slide2/5e9b3faa87857414dcd1eb40.jpg","image_slide_3":"https://res.cloudinary.com/elesarr/image/upload/v1587235994/campaign/individual/slide3/5e9b3faa87857414dcd1eb40.jpg","pitch_deck":"https://res.cloudinary.com/elesarr/image/upload/v1587241016/campaign/individual/pitchDeck/5e9b3faa87857414dcd1eb40.pdf","fb_url":"http://elesarr.com/","linkedin":"http://elesarr.com/","inst_url":"http://elesarr.com/","video_url":"https://www.youtube.com/watch?v=SGX5o4b7VU8",
// "project_deadline":{"$date":{"$numberLong":"1588377600000"}},"project_goal":"2000",
// "email":"info@elesarr.com",
// "username":"aragontech","category":"Creatives","__v":{"$numberInt":"0"}}