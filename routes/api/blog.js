require('dotenv').config()
const express = require('express');
const router = express.Router();

const {Post,validate}  = require('../../model/Post');
const {BlogCategory} = require("../../model/BlogCategory")
const {Comment} = require("../../model/Comment")
var _ = require('lodash');
var path = require('path');
var multer = require('multer');

const api_Key = process.env.MAILGUNKEY
const domain = 'elesarr.com';
var mailgun = require('mailgun-js')({apiKey: api_Key, domain: domain});
const auth = require("../../middleware/auth")
const admin = require("../../middleware/admin")
const staff = require("../../middleware/staff")
const writer = require("../../middleware/writer")

router.get("/posts",async(req, res)=> {
  try{
    const posts = await Post.find()
                            .populate("owner")
                            .populate("comments")
    res.json(posts)
  }catch(e){
    return res.status(400).json({msg:e.message})
  }
})


router.get("/post/recent", async(req, res) => {
  try{
    const posts = await Post.find().limit(Number(3)).sort({dateCreated: -1}).populate("comments")
    res.json(posts)
  }catch(e){
    return res.status(400).json({msg:e.message}) 
  }
})



router.get("/posts/length",auth, admin, async(req, res)=> {
  try{
    const posts = await Post.find()
    res.json(posts.length)
  }catch(e){
    return res.status(400).json({msg:e.message})
  }
})



router.get("/posts/:slug",(req, res)=> {
    Post.findOne({slug:req.params.slug})
        .populate("owner").populate({
          path:"comments",
          populate:{path:"owner"}
        })
        .then(post => {
            if(!post){
                return res.json({message:"POST DOSEN'T EXISTS " })
            }
            return res.json(post)
        })  
})

router.get("/categories", async(req, res) => {
  try{
    const categories = await BlogCategory.find()
    res.json(categories)
  }catch(e){
    return res.status(400).json({msg:e.message})
  }
}); 

const cloudinary = require("cloudinary");
cloudinary.config({
  cloud_name: "elesarr",
  api_key: "767878828592729",
  api_secret: "vnd_jkBSI05Y0_YzNxwgpoJo1rI"
});
  
const storage = multer.diskStorage({
 
  filename: (req, file, cb) => {
    const { id } = req.params
    cb(null, `profile-files-UserId-${id}-Image-${Date.now()}.png`)
  }
  })
  
  const fileFilter =(req, file, cb)=>{
    // reject a file
    if(file.mimetype === 'image/jpeg' || file.mimetype == "image/jpg" || file.mimetype == 'image/png' ){
      cb(null, true)
    }else{
      cb(null, false)

    }
  } 
    
  const upload = multer({ 
    storage:storage,
    limits:{
      fileSize:1024*1024*5
    },
    fileFilter:fileFilter
})



router.post("/posts",auth,admin,upload.single("image"),async(req, res)=> {
    const {title, content} = req.body
    var newTitle = title.replace(/[\|&;\$%@".:<>\(\)\+,]/g, "");
    console.log("NEW TITLE", newTitle)
    /** VALIDATE PART */
    const {error} = validate(req.body)
    if(error) return res.status(400).json(error.details[0].message)
    /** END VALIDATE PART */
    // const {image}  = req.file
    var image;
    await cloudinary.v2.uploader.upload(
        req.file.path,
        // {public_id:`${id}/blog/${id}-profile`}, 
        function(err, result) {
        
          if (err) {
            res.json(err.message);
          }else{
            // IMAGEARR.push(result.secure_url)
            // image3  = result.secure_url
            image  = result.secure_url
    
            // return res.json(result)
            const newPost = new Post({
                title:newTitle, 
                content:content, 
                owner:req.user.id, 
                category:req.body.category,
                image
            })      
            newPost.save()
            .then(post=> res.json(post))
            .catch(e =>{
              return res.status(400).json({msg:e.message})
            })
        }
      })

})




router.get("/post/:slug/comment", (req, res)=> {
    Post.findOne({slug:req.params.slug}).populate("comments")
        .then(post => {
            if(!post){
                return res.json({message:"NO POST CREATED"})
            }
            return res.json(post)
        })
})


router.post("/post/:slug/comment", (req, res)=>{
    const {content, post, owner} = req.body
    console.log(content)
    console.log(post)
    console.log(owner)
    // console.log(fullName)
    
    const newComment = new Comment({
        content, post, owner
    })
    newComment.save()
              .then(comment=>{
                  
                  Post.findOneAndUpdate({slug:req.params.slug},
                    { $push: { comments: comment._id } },
                    { new: true, useFindAndModify: false }
                    )
                    .then((post)=>{
                        return res.json(post)
                    }) 
              })
})




/** WRITER SECTION  */

router.get("/writer-get-posts", auth, writer, async(req, res) => {
  try{
    const posts = await Post.find()
                            .where("owner")
                            .equals(req.user.id)
    res.json(posts)
  }catch(e){
    return res.status(400).json({msg:e.message})
  }
})




/** WRITER CREATE POST SECTION */


router.post("/writer-create-posts",auth,writer,upload.single("image"),async(req, res)=> {
  const {title, content} = req.body
  var newTitle = title.replace(/[\|&;\$%@".:<>\(\)\+,]/g, "");
  console.log("NEW TITLE", newTitle)
  /** VALIDATE PART */
  const {error} = validate(req.body)
  if(error) return res.status(400).json(error.details[0].message)
  /** END VALIDATE PART */
  // const {image}  = req.file
  var image;
  await cloudinary.v2.uploader.upload(
      req.file.path,
      // {public_id:`${id}/blog/${id}-profile`}, 
      function(err, result) {
      
        if (err) {
          res.json(err.message);
        }else{
          // IMAGEARR.push(result.secure_url)
          // image3  = result.secure_url
          image  = result.secure_url
  
          // return res.json(result)
          const newPost = new Post({
              title:newTitle, 
              content:content, 
              owner:req.user.id, 
              category:req.body.category,
              image
          })      
          newPost.save()
          .then(post=> res.json(post))
          .catch(e =>{
            return res.status(400).json({msg:e.message})
          })
      }
    })

})




/** WRITER EDIT POST SECTION */
router.put("/writer-edit-posts/:slug", auth, writer,upload.single("image"), async(req, res) => {
  try{
    const {title, content} = req.body
    var newTitle = title.replace(/[\|&;\$%@".:<>\(\)\+,]/g, "");
    // console.log("NEW TITLE", newTitle)
    /** VALIDATE PART */
    const {error} = validate(req.body)
    if(error) return res.status(400).json(error.details[0].message)
    /** END VALIDATE PART */
    // const {image}  = req.file
    var image;
    await cloudinary.v2.uploader.upload(
        req.file.path,
        // {public_id:`${id}/blog/${id}-profile`}, 
        function(err, result) {
        
          if (err) {
            res.json(err.message);
          }else{
            // IMAGEARR.push(result.secure_url)
            // image3  = result.secure_url
            image  = result.secure_url
            Post.findOneAndUpdate({slug:req.params.slug}, {
              $set:{
                'title':newTitle, 
                'content':content, 
                'owner':req.user.id, 
                'category':req.body.category,
                'image':image
              }
            })
            .then(post=> res.json(post))
            .catch(e =>{
              return res.status(400).json({msg:e.message})
            })

        }
      })
  }catch(e){
    return res.status(400).json({msg:e.message})
  }
})





/** WRITER DELETE POST */
router.delete("/writer-delete-post/:slug", auth, writer, async(req, res) => {
  try{
    const post = await Post.findOneAndRemove({slug:req.params.slug})
    res.json(post)
  }catch(e){
    return res.status(400).json({msg:e.message})

  }
})

/** WRITER DELETE POST */


/** WRITER SINGLE POST */

router.get("/writer-post/:slug",auth,writer,async(req, res)=> {
  try{
      const posts =   await Post.findOne({slug:req.params.slug})
                          
                          .populate("owner").populate({
                            path:"comments",
                            populate:{path:"owner"}
                          })
      res.json(posts)
  }catch(e){
    return res.status(400).json({msg:e.message})
  }
})

/*** GET WRITER POST LENGTH */
router.get("/writer-get-posts-length", auth, writer, async(req, res) => {
  try{
    const posts = await Post.find()
                            .where("owner")
                            .equals(req.user.id)
    res.json(posts.length)
  }catch(e){
    return res.status(400).json({msg:e.message})
  }
})

module.exports = router;
