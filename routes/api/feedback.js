require('dotenv').config()
const express = require('express');
const router = express.Router();
const request= require("request")

const api_Key = process.env.MAILGUNKEY
const domain = 'elesarr.com';
var mailgun = require('mailgun-js')({apiKey: api_Key, domain: domain});

// Feedback Model
const {Feedback, validateFeedback}=require("../../model/Feedback") 


router.post("/", (req, res)=> {
    /** VALIDATE PART */
    const {error} = validateFeedback(req.body)
    if(error) return res.status(400).json(error.details[0].message)
    /** END VALIDATE PART */
    const{email, text, firstName, lastName}= req.body
    // console.log(email)
    // console.log(text)
    // console.log(firstName)
    // console.log(lastName)
    const newFeedback = new Feedback({
        email, 
        firstName,
        lastName,
        text
    })

    if(!email || !firstName || !lastName || !text){
        return res.status(400).json({ msg: "Please enter your email" });
    }

    const data = {
        members:[
          {
            email_address:email,
            status:"subscribed",
            merge_fields:{
                FNAME:firstName,
                LNAME:lastName
            }
          }
        ]
      }
      const postData = JSON.stringify(data) 
      const options = {
        url :"https://us19.api.mailchimp.com/3.0/lists/02e1d16e87",
        method:'POST',
        headers:{
          Authorization:"auth e56abf2dd98943505e8fcaee5e36ea02-us19"
        },
        body:postData
      };

      request(options, (err, response,body)=>{
        if(err){
          console.log("MAILCHIMP: ERROR", err)
        } else{
          if(response.statusCode === 200){
            console.log("SUCCESS")
          } else {
            console.log("FAILED")
          }
        }
      })
      
    newFeedback.save()
                .then((feedback) => {
                    res.send(feedback)
                })
                .catch((e) => {
                  return res.status(400).json({msg:e.message})
                }) 
})


router.get("/", async(req, res) => {
  try{
    const feedback = await Feedback.find()
    res.json(feedback)
  }catch(e){
    return res.status(400).json({msg:e.message})
  }
})


router.get("/length", async(req, res) => {
  try{
    const feedback = await Feedback.find()
    res.json(feedback.length)
  }catch(e){
    return res.status(400).json({msg:e.message})
  }
})


module.exports = router;
