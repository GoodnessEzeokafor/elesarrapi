require('dotenv').config()


/** IMPORT MODULES */
const express = require("express");   // import express
const router = express.Router();  // import router
const bcrypt = require("bcryptjs");  // import bcrypt
const jwt = require("jsonwebtoken"); // import JWT
const fs = require('fs'); // import fs
const request= require("request")
const Badge = require("../../model/Badge")
const {Notification} = require("../../model/Notification")

/** END IMPORT MODULES */



/** PUSHER JS CONFIGURATION */
var Pusher = require("pusher");
var pusher = new Pusher({
  appId: '1004110',
  key: '2347f2ea036eda8b245e',
  secret: 'b072af717d87f7f792a5',
  cluster: 'eu',
  useTLS: true,
});

/**  PUSHER JS CONFIGURATION */



/*
  ROUTE FOR NORMAl USERS
*/


var _ = require('lodash'); // import loadash
var multer = require('multer'); // import multer for uploading images



/** IMPORT AUTH CONFIG */
const auth = require("../../middleware/auth") // import auth middleware
const admin = require("../../middleware/admin")
/** IMPORT AUTH CONFIG */


/** MODELS */
const {User} = require("../../model/SignupUser") // import Individual user table
/** END MODELS */



/** SENDGRID CONFIG */
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);
/** SENDGRID CONFIG */



/** MAILGUN CONFIG */
const api_Key = process.env.MAILGUNKEY
const domain = 'elesarr.com';
var mailgun = require('mailgun-js')({apiKey: api_Key, domain: domain});
/** MAILGUN CONFIG  */


/** CLOUDINARY CONFIG */
const cloudinary = require("cloudinary");
cloudinary.config({
  cloud_name: "elesarr",
  api_key: "767878828592729",
  api_secret: "vnd_jkBSI05Y0_YzNxwgpoJo1rI"
});
  
const storage = multer.diskStorage({
  filename: (req, file, cb) => {
    const { id } = req.params
    cb(null, `profile-files-UserId-${id}-Image-${Date.now()}.png`)
  }
  })
  
  const fileFilter =(req, file, cb)=>{
    // reject a file
    if(file.mimetype === 'image/jpeg' || file.mimetype == "image/jpg" || file.mimetype == 'image/png' ){
      cb(null, true)
    }else{
      cb(null, false)

    }
  } 
    
  const upload = multer({ 
    storage:storage,
    limits:{
      fileSize:1024*1024*5
    },
    fileFilter:fileFilter
})

/** CLOUDINARY CONFIG */

/**
 * @method POST
 * @route  api/users
 * @description ENDPOINT FOR CREATING NEW USERS
 * @param  
 * @docs - written by Goodness Ezeokafor - CEO/LEAD DEVELOPER
 */



router.post("/", (req, res) => {
  /**
   * @req.body
   *     username, email, password, confirmPassword   
   */
  const { username, email, password,confirmPassword} = req.body;  
  
  // Simple validation
  if (!username || !email || !password ||!confirmPassword) {
    /** 
     * CHECKS IF ALL FIELD WERE FILLED
     */
    return res.status(400).json({ msg: "PLEASE ENTER ALL FIELDS" });
  }
  
  if(password !== confirmPassword){
    /** CHECKS IF PASSWORD AND CONFIRM PASSWORD ARE THE SAME */
    return res.status(400).json({ msg: "PASSWORDS MUST MATCH" });
  }

  // Check for existing user
  User.findOne({ email }).then(user => {
    /** 
     * CHECKS IF THE USER ALREADY EXISTS 
     * */
    if (user) return res.status(400).json({ msg: "User already exists" });
    var lowerEmail = email.toLowerCase
    const newUser = new User({
      username,
      email,
      password,
    }); // construct new user 
    const msg = {
      to: `${newUser.email}`,
      from: 'Emmanuel From Elesarr <goodness@elesarr.com>',
      subject: 'HAPPY TO HAVE YOU',
      // text: 'and easy to do anywhere, even with Node.js',
      html:`<h5>Thanks For Signing Up On Our Platform!!!. </h1>
      <p>Hi, this is the Elesarr team and we want to appreciate you for signing up on Elesarr</p>
      <p>Click <a href="https://elesarr.com/login">here</a> to start a campaign</p>
      <p>If you encounter any issues you can email us at customercare@elesarr.com and our team will respond to you in no time</p>
      <p>THANK YOU!!!</p>
    `,
    };
    /** MAILCHIMP */
    const data = {
      members:[
        {
          email_address:email,
          status:"subscribed",
          merge_fields:{
              FNAME:username,
          }
        }
      ]
    }
    const postData = JSON.stringify(data) 
    const options = {
      url :"https://us19.api.mailchimp.com/3.0/lists/02e1d16e87",
      method:'POST',
      headers:{
        Authorization:"auth e56abf2dd98943505e8fcaee5e36ea02-us19"
      },
      body:postData
    };

    request(options, (err, response,body)=>{
      if(err){
        console.log("MAILCHIMP: ERROR", err)
      } else{
        if(response.statusCode === 200){
          console.log("SUCCESS")
        } else {
          console.log("FAILED")
        }
      }
    })
    
    /** END MAILCHIMP */

    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(newUser.password, salt, (err, hash) => {
        if (err) throw err;
        newUser.password = hash;
        newUser.save().then(user => {
          jwt.sign(
            { id: user.id, isAdmin:user.isAdmin, isWriter:user.isWriter, isStaff:user.isStaff},
            process.env.jwtSECRET,
            { expiresIn: 10000 },
            (err, token) => {
              if (err) throw err;
              /** SEND MAIL */              
              mailgun.messages().send(msg, function (error, body) {
                if (error){
                  console.log(error);
                }
                console.log("EMAIL SENT!!!");
              });
              /** SEND MAIL */

              /** REWARD MEMBERSHIP BADGE */
              const badge = new Badge({
                user_email:user.email,
                badgeurl:"https://res.cloudinary.com/elesarr/image/upload/v1593793062/badges/ElesarrBadge2_yxekpv.png",
                status:"membership",
                // user_id:user.id
              }) // create badge 
              badge.save()
                    .then((badge) => {
                      const notifData = new Notification({
                        ownerID: user.id,
                        message: `
                          You have been rewarded with a membership badge
                          `,
                      });
                      notifData.save();
                        pusher.trigger('Elesarr', 'MEMBERSHIP_BADGE_REWARD', {
                          // username:username,
                          userID: user.id,
                          // project_name: project_name,
                        });
                    })
                    .catch((e) => {
                      res.json(e.message)
                    })
              /** REWARD MEMBERSHIP BADGE */

              res.json({
                token,
                // user: {
                  id: user.id,
                  username: user.username,
                  email: user.email,
                  class:user.class,
                  date : user.timestamp,
                  profile:user.profile,
                  isAdmin:user.isAdmin,
                  isWriter:user.isWriter,
                  verified :  user.verified
                // }
              });
            }
          )
        });
      });
    });
  }).catch(err =>{
    return res.status(400).json({ msg: "ERROR Contact Administrator!!! or email customercare@elesarr.com" });
  }) 
});


/**
 * @method POST
 * @route  api/users
 * @description ENDPOINT FOR VERIFYING TOKEN
 * @param  
 * @docs - written by Goodness Ezeokafor - CEO/LEAD DEVELOPER
 */

router.get('/oneUser', auth, (req,res) =>{
  User.findById(req.user.id)
  .select("-password")
  .then(user => {
    
      /** INDIVIDUAL ACCOUNT */
      return res.json( {
        id: user.id,
        username: user.username,
        email: user.email,
        class:user.class,
        date : user.timestamp,
        isAdmin:user.isAdmin,
        isWriter:user.isWriter,
        profile:user.profile,

      });
     
  }) 
})







/*
  CREATE USER PROFILE
*/
// @route   update api/signupuser/user/:id/profile
// @desc    create user profile for Individual account 
// @access  Public
// @method POST

/**
 * @route  /user/:id/profile
 * @desc route for creating user profile
 * @method PUT 
 */


router.get("/user/profile", auth,(req, res)=>{
  User.findById(req.user.id)
      .then((user) =>{
        if(user.profile){
          res.json(user.profile)
        }else{
          res.json([])
        }
      })
      .catch((err) =>{
        return res.status(400).json({ msg: err.message});
      })
})
// 5f16e605840c2b43d34e86e8

router.put("/user/:id/profile",auth,upload.single("profilePicture"),async(req, res) => {
  var id = req.params.id;
  const {
    firstName, 
    lastName,
    nationalIdentityCardNumber, 
    address,
    country,
    state,
    dateOfBirth,
    occupation,
    telephone
  } = req.body // get form data body

  const {profilePicture}  = req.file  // get form image 
  
  if (!firstName || !telephone || !lastName || !nationalIdentityCardNumber ||!address || !country || !state || !dateOfBirth || !occupation) {
    /** CHECKS IF ALL THE FIELDS ARE FILLED */
    return res.status(400).json({ msg: "PLEASE ENTER ALL FIELDS" });
  }

  /**  VERIFY NIN NUMBER  */
  /**VERIFY NIN NUMBER  */
  
  /** CLOUDINARY IMPLEMENTATION */
  await cloudinary.v2.uploader.upload(
    req.file.path,
    {public_id:`profile/${id}/${id}-profile`}, 
    function(err, result) {
      /** UPLOADING PROFILE PIC */  
      if (err) {
        res.json(err.message);
      }else{
        profilePic  = result.secure_url
        /** UPDATE USER TABLES WITH PROFILE INFO */
    User.findByIdAndUpdate(id, {
      $set: {
        "profile.firstName" : firstName,
        "profile.lastName" : lastName,
        "profile.profilePicture" : profilePic,
        "profile.nationalIdentityCardNumber" : nationalIdentityCardNumber,
        "profile.address" : address,
        "profile.country":country,
        "profile.state":state,
        "profile.dateOfBirth":dateOfBirth,
        "profile.occupation":occupation,
        "profile.telephone":telephone
      }
    })
    .then((user)=> {
      res.json(user)
    })
      .catch((err) =>{
        return res.status(400).json({ msg: err.message });
      });
    }
  })
});



/**
 * @route  /user/:id/profile/edit
 * @desc route for edting profile image
 * @method PUT 
 */


router.put("/user/:id/profile/edit",upload.single("profilePicture"), async(req, res) => {
  var id = req.params.id;
  const profilePicture  = req.file
  // console.log("PROFILE PICTURE:",profilePicture)
  if (!profilePicture) {
    return res.status(400).json({ msg: "Please enter all fields" });
  }

  await cloudinary.v2.uploader.upload(
    req.file.path,
    {public_id:`profile/${id}/${id}-profile`}, 
    function(err, result) {
    
    if (err) {
    res.json(err.message);
    }else{
      profilePic  = result.secure_url

      User.findByIdAndUpdate(id, {
        $set: {
          "profile.profilePicture" : profilePic,
        }
      })
        .then(() =>  User.findById(id)
        ).then(user => res.json({updated : user}))
        .catch(err =>{
          return res.status(400).json({ msg: err.message});
        });    
      }
  })
});





/*
 USER DASHBORD
*/

// 5f16e605840c2b43d34e86e8
router.put("/set-admin", auth,(req, res) => {
  var id = req.user.id;
  User.findByIdAndUpdate(id, {
    $set: {
      "isAdmin": true,
      'class':"Admin"
      // account: req.body.account
    }
  })
    .then(() => res.json({ success: true }))
    .catch((err) =>{
      if(err){
        return res.status(400).json({ msg: err.message});
      }
    });
});



router.get("/:id/tx", (req, res)=>{
  User.findById(req.params.id)
      .then(payments => res.json(payments.payments))
      .catch(err => res.status(404).json({failed:err}))
})



/** GET ALL USERS */
router.get("/all/length", async(req, res) => {
  try{
      const users = await User.find()
      res.json(users.length)
  }catch(e){
    return res.status(400).json({ msg: err.message});

  }

});



/** BE A WRITER */
router.put("/set-writer",auth,async(req, res) => {
  try{
    const user = await User.findByIdAndUpdate(req.user.id, {
      $set:{
        "isWriter":true
      }
    })
    res.json(user)
  }catch(e){
    return res.status(400).json({ msg: e.message});
  }
})





/** */








module.exports = router;
