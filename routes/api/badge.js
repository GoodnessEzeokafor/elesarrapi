const express = require('express');
const router = express.Router();
const Badge = require("../../model/Badge")

router.post("/",(req,res) => {
    const {user_email, badgeurl, status} = req.body
    const newBadge = new Badge({
        user_email, badgeurl, status
    })
    newBadge.save()
            .then((badge) => {
                res.json(badge)
            })

        .catch((e) => {
            if(e) return res.json(e.message)
        })
})



router.get("/",(req,res) => {
    Badge.find()
        .then((badges) => {
            res.json(badges)
        })
        .catch((e) => {
            if(e) return res.json(e.message)
        })
})




router.get("/:email/badges",(req,res) => {
    Badge.find({user_email:req.params.email})
        .then((badges) => {
            res.json(badges)
        })
        .catch((e) => {
            if(e) return res.json(e.message)
        })
})
module.exports = router;
