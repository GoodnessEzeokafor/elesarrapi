const express = require('express')
const router = express.Router()


const Item = require("../../model/Item")



router.get('/', (req, res) => {
    Item.find()
        .lean()
        .sort({date:1})
        .then(items => res.json(items))
})
router.post("/", (req, res) => {
    const newItem = new Item({
        name:req.body.name
    })

     newItem.save()
        .then((item)  => res.json(item))
})

// delete
router.delete('/:id', (req, res) => {
    Item.findById(req.params.id)
        .then(item => item.remove().then(() => res.json({
            message:"Successfully deleted the item!!",
            type:"success"
        })))

        .catch(err => res.status(404).json({
            message:"Item Not Deleted"
        }))
})
module.exports = router