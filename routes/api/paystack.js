const express = require("express");
const router = express.Router();
const _ = require("lodash");
const request = require("request");
const { initializePayment, verifyPayment } = require("../../config/paystack")(
  request
);
const {Guest} = require("../../model/Guest")

const Pay = require("../../model/Pay");
const {User} = require("../../model/SignupUser");

const {Project} = require("../../model/Project");
const {Notification} = require("../../model/Notification")

/** PUSHER JS CONFIGURATION */
var Pusher = require("pusher");
var pusher = new Pusher({
  appId: '1004110',
  key: '2347f2ea036eda8b245e',
  secret: 'b072af717d87f7f792a5',
  cluster: 'eu',
  encrypted: true
});




router.post("/callback", async(req, res) => {
  const ref = req.query.reference;
  // console.log(ref)

  console.log("Callback stage")
  verifyPayment(ref, async(error, body) => {
    if (error) {
      console.log(error);
      return res.json({ msg: error });
    }
    response = JSON.parse(body);
    // console.log("Verify payment response", response.data);

    const { reference, amount } = response.data;
    const { email } = response.data.customer;
    const { full_name, projectID } = response.data.metadata;
    newGuest = { reference, amount, email, full_name,projectID};
    try{

      const guest = new Guest(newGuest)  
      await guest.save()
      const project = await Project.findOneAndUpdate({slug:projectID},{
        $inc: {
          project_current_balance: guest.amount / 100,
        },
      })
      const notif = new Notification({
        ownerID : project.userID,
        message : `Your Campaign Received A Donation Of ${amount/100} from ${email} `,
        
      })

      await notif.save()

      const pusherData ={
        ownerID: project.userID
      }
      // PUSHER TO TRIGGER A NOTIFICATION TO THE OWNER OF THE PROJECT
      pusher.trigger("Elesarr", "PROJECT_FUNDED", {
        pusherData,
      });  // trigger events
      // res.json(project)
      return res.redirect("https://elesarr.com/thanks")
    }catch(e){
      return res.status(400).json({ msg: e.message});
    }

  });
});

// Update the users amount after paying with paystack

router.put("/update/:id", (req, res) => {
  var id = req.params.id;
  console.log(id);

  User.findByIdAndUpdate(id, {
    $set: {
      "local.money": req.body.account,
    },
  })
    .then(() => res.json({ success: true }))
    .catch((err) => res.status(204).json({ success: err }));
});

// Get all transactions

router.get("/transactions", (req, res) => {
  Pay.find()
    .then((tx) => res.json({ success: tx }))
    .catch((err) => res.status(204).json({ success: err }));
});



module.exports = router;
