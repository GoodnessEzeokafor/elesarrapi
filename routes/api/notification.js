const {Notification}  = require("../../model/Notification")
const express = require("express");
const router = express.Router();

var Pusher = require("pusher");

var pusher = new Pusher({
  appId: '1004110',
  key: '2347f2ea036eda8b245e',
  secret: 'b072af717d87f7f792a5',
  cluster: 'eu',
  encrypted: true
});




router.get("/", (req,res)=>{
 
  


  Notification.find().sort({date: -1}).then((data) =>{
    res.status(200).json(data)
  })
})



router.get("/:ownerID", (req,res)=>{
    const ownerID = req.params.ownerID
    console.log("owner id", ownerID)



    Notification.find({ownerID : ownerID}).sort({date: -1}).then((data) =>{
      res.status(200).json(data)
    })
  })

  // when the user sees the notification

  router.post("/seen/:notifID", (req,res)=>{
    const notifID = req.params.notifID
    console.log("notif id", notifID)
    Notification.findByIdAndUpdate(notifID, {
      $set: {
        seen : true
      }
    }).then(data =>{

  
   
      pusher.trigger("Elesarr", "notifSeen", {
        notifID: notifID
      })
      res.json("Successful")
    }

   
    )

  })


  router.post("/delete/all" , (req,res)=>{
  Notification.remove({}).then(()=>{
    res.json("successful")
  })
})


module.exports = router