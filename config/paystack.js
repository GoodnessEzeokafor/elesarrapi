const paystack = request => {

  
    const MySecretKey =  `Bearer sk_live_3fa7f16367c22d5846134ebdc6d9f96c2ac97aaa`;
    
    //replace the secret key with that from your paystack account
    const initializePayment = (form, mycallback) => {
      const options = {
        url: "https://api.paystack.co/transaction/initialize",
        headers: {
          authorization:   MySecretKey,
          "content-type": "application/json",
          "cache-control": "no-cache"
        },
        form
      };

      console.log(options)
      const callback = (error, response, body) => {
        return mycallback(error, body);
      };
      request.post(options, callback);
    };
  
    const verifyPayment = (ref, mycallback) => {
      const options = {
        url:
          "https://api.paystack.co/transaction/verify/" + encodeURIComponent(ref),
        headers: {
          authorization: MySecretKey,
          "content-type": "application/json",
          "cache-control": "no-cache"
        }
      };
      const callback = (error, response, body) => {
        return mycallback(error, body);
      };
      request(options, callback);
    };
  
    return { initializePayment, verifyPayment };
  };
  
  module.exports = paystack;