require("dotenv").config();



var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var dotenv = require('dotenv').config();

const express = require("express");

const cors = require("cors");

// const config = require('config')
const path = require("path");
// const mailer = require('./misc/mailer')

const mongoose = require("mongoose");

const app = express();



app.use(
  cors({
    // origin: ["http://localhost:3000", "http://127.0.0.1:3000"],
    credentials: true
  })
);

app.use(express.json());

// mongoose uri

const db = process.env.MONGO_URL_TEST;

// Connect to mongoose
// console.log(db)

try{
  mongoose
  .connect(db, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  useFindAndModify : false
  })
  .then(() => console.log("MongoDB Connected..."))
  .catch((err) =>{
    if(err){
      // return res.send({msg:err})
      console.log("error ", err)
    }
  });

}catch(e){
  if(e){
  return e
  }
}

app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// app.use(express.static(path.join(__dirname, "public")));

// app.use(express.static('/uploads', express.static('uploads')))
app.use('/uploads', express.static('uploads'));


app.use("/api/login", require("./routes/api/auth"));

// app.use("/api/signupusers", SignupUsersRoutes);
app.use("/api/signupuser", require("./routes/api/signupuser"));
app.use("/api/projects", require("./routes/api/project"));
app.use("/api/admin",require("./routes/api/admin"));
app.use("/api/paystack", require("./routes/api/paystack"))
app.use("/api/feedback", require("./routes/api/feedback"))
app.use("/api/payout", require("./routes/api/payout"))
app.use("/api/verify", require("./routes/api/verify"))
app.use("/api/paystack/guest", require("./routes/api/guest"))
app.use("/api/flutterwave", require("./routes/api/flutterwave"))
app.use("/api/blog", require("./routes/api/blog"))
app.use("/api/badge", require("./routes/api/badge"))

app.use("/api/notifications", require("./routes/api/notification"))
app.use("/api/newsletter", require("./routes/api/newsletter"))
app.use("/api/staff", require("./routes/api/staff"))


app.set("port", process.env.PORT || 5200);
const server = app.listen(app.get("port"), () => {
  console.log(`Express running → PORT ${server.address().port}`);
});


// // nodeadmin000
// test@email.com


/** DON"T TOUCH 
 * 
 * 
MONGO_URL='mongodb+srv://ElessarAdmin:r8N1xFAUKridhcTN@cluster0-8gelz.mongodb.net/Elesarr?retryWrites=true&w=majority'



DEV
MONGO_URL='mongodb://iammanuel:Loaded888@cluster0-shard-00-00-r5sjo.mongodb.net:27017,cluster0-shard-00-01-r5sjo.mongodb.net:27017,cluster0-shard-00-02-r5sjo.mongodb.net:27017/<dbname>?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority'

 * 
*/
