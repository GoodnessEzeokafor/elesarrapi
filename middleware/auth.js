// const config = require('config');
require('dotenv').config()

const jwt = require('jsonwebtoken');



module.exports = function(req, res, next){
  const token = req.header('X-auth-token')
  if(!token) return res.status(401).send('Access denied. No token provided')
  try{
      const decoded = jwt.verify(token, process.env.jwtSECRET);
      req.user = decoded
      console.log("AUTH FILE", req.user)
      console.log("TOKENS", token)
      next()
  }catch(e){
      res.status(400).send('invalid token')
  }
}   




// function auth(req, res, next) {
//   const token = req.header('x-auth-token');
//   console.log("token" , token)

//   // Check for token
//   if (!token)
//     return res.status(401).json({ msg: 'No token, authorizaton denied' });

//   try {
//     // Verify token
//     const decoded = jwt.verify(token,process.env.jwtSECRET);
//     // Add user from payload
//     req.user = decoded;
//     next();
//   } catch (e) {

//     return res.status(401).json({ msg: e.message});
 
//     // res.status(400).json({ msg: e });
//   }

  
// }

// module.exports = auth;