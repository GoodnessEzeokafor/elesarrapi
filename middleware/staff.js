module.exports = function(req, res,next){

    // 401 unauthorized
    // 403 Forbidden
    if (!req.user.isStaff) return res.status(403).json({ msg: 'Access denied. NOT A STAFF' });
    next()
}