// const config = require('config');
require('dotenv').config()

const jwt = require('jsonwebtoken');

function redirectAuth(req, res, next) {
  const token = req.header('x-auth-token');
  console.log("token" , token)

  // Check for token
  if (!token)
    return res.redirect("http://elesarr.com/login")
    // return res.status(401).json({ msg: 'No token, authorizaton denied' });

  try {
    // Verify token
    const decoded = jwt.verify(token,process.env.jwtSECRET);
    // Add user from payload
    req.user = decoded;
    next();
  } catch (e) {


  return res.json({
    message:'error'
  })    
    // res.status(400).json({ msg: e });
  }

  
}

module.exports = redirectAuth;