const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

const NewsLetterSchema = new Schema({   

    email:{
        type:String, 
        required:true    
    },

    date_created:{
        type:Date,
        default:Date.now
    },

    date_updated:{
        type:Date
    }
})


function validateNewsletter(newsletter){
    const schema={
        email: Joi.string().min(5).max(255).required().email()
    }
    return Joi.validate(newsletter, schema)
}

module.exports = {
    Newsletter : mongoose.model("newsletter", NewsLetterSchema),
    validateNewsletter:validateNewsletter
}
