const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema

const NotificationSchema = new Schema({

  ownerID: {
    type:Schema.Types.ObjectId,
    ref:'signupuser'

  },

  message: {
    type: String    
  },

  link : {
    type : String
  },

  seen : {
    type : Boolean,
    default : false
  },

  date : {
      type : Date,
      default : Date.now
  }


});



function validateNotification(notification){
  const schema={
    ownerID:Joi.objectId().required(),
    message:Joi.string().min(5).max(120).required(),
    link:Joi.string().min(5).max(120).required(),
    
      // reference:Joi.string().min(5).max(120).required()
  }
  return Joi.validate(notification, schema)
}





module.exports = {
  Notification : mongoose.model("notification", NotificationSchema),
  validateNotification:validateNotification
}



// module.exports = Notification = mongoose.model("notification", NotificationSchema);
