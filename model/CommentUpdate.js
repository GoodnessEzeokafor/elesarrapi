const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)



const commentUpdateSchema = new mongoose.Schema({
    content: {
        type: String,
        required: true,
    },

    likes: {
        type: Number,
        default: '0',
    },

    update:{
        type:Schema.Types.ObjectId,
        ref:'update'
    },

    owner:{
        type:Schema.Types.ObjectId,
        ref:'signupuser'
    },

    dateCreated:{
        type:Date,
        default:Date.now
    },

    dateUpdated:{
        type:Date
    }
    
});


function validateCommentUpdate(comment){
    const schema={
        content:Joi.string().min(5).max(120).required(),
        likes:Joi.number(),
        post:Joi.objectId().required(),
        owner:Joi.objectId().required()
    }
    return Joi.validate(comment, schema)
}
module.exports = {
    CommentUpdate : mongoose.model('update_comments', commentUpdateSchema),   
    validateCommentUpdate:validateCommentUpdate
}

// const CommentUpdate = mongoose.model('update_comments', commentUpdateSchema);
// module.exports = CommentUpdate
