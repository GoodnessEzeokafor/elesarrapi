const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const slugify = require('slugify')



const ProjectFaqSchema = new mongoose.Schema({
    question: {
        type: String,
        required: true,
    },
    answer: {
        type: String,
        required: true,
    },
    project:{
        type:Schema.Types.ObjectId,
        ref:'ProjectSchema'
    },
    date_created:{
        type:Date,
        default:Date.now
    },
    date_updated:{
        type:Date,
    },

});


// 5f1986c49d71440017b9cc47

function validateProjectFaq(faq){
    const schema = {
        question:Joi.string().min(5).max(300).required(),
        answer: Joi.string().min(5).max(300).required(),
        project:Joi.objectId().required(),
    }
    return Joi.validate(faq, schema)
}



// 

// module.exports = ProjectFaq = mongoose.m/odel('ProjectFaq', ProjectFaqSchema) 
module.exports ={
    ProjectFaq: mongoose.model('project_faq', ProjectFaqSchema),
    validateProjectFaq:validateProjectFaq
}
