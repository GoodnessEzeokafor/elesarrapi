/**
 * 
 * @Developer - Goodness Chinemerem Ezeokafor
 * @position - CEO/LEAD DEVELOPER
 * @model - PAY
 * @DocumentationDate - 21st June 2020
 * @fields - 
 * 
 * 
 *  MODEL NOT IN USE RIGHT NOW

 */
const mongoose = require('mongoose')


const paySchema = new mongoose.Schema({
    full_name: {
        type: String,
        required: true,
    },

    email: {
        type: String,
        required: true,
    },
    amount: {
        type: Number,
        required: true,
    },
    method : {
        type : String,
        // required : true
    },
    
    userID : {
        type : String,
        // require : true
    },
    projectID : {
        type : String,
        required: true
    },

    reference: {
        type: String,
        required: true
    },
    datePaid:{
        type:Date,
        default:Date.now

    }
});

const Pay = mongoose.model('Pay', paySchema);

module.exports = Pay 