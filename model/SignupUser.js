require('dotenv').config()
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)



const SignupUserSchema = new Schema({
    username:{
        type:String,
        // unique:true,
        lowercase:true,
        required:true,
        index:true
    },
    email: {
      type: String,
      // unique:true,
      lowercase : true,
      required : true,
      index:true
    },
    class : {
      type :String,
      default : "Individual"
    },
    password: {
      type: String,
      required: true,
    },
    timestamp:{
      type:Date,
      default:Date.now
    },
    flag:{
      type:Boolean,
      default:false
    },
    isAdmin:{
      type:Boolean,
      default:false
    },
    isStaff:{
      type:Boolean,
      default:false
    },
    isWriter:{
      type:Boolean,
      default:false
    },
    payments : {
      type : Array,
      default : []
    },

    verified : {
      type : Boolean,
      default : false
    },
    profile : {
      firstName:{
        type:String,
        // default : ""/
        // required:true,
      },
      lastName:{
        type:String,
        // required:true,
        // default : ""
      },
      profilePicture:{
        type:String  
      },
      nationalIdentityCardNumber:{
        type:String,
        // required:true,
        // unique:true,
        // default : ""
      },
      address:{
          type:String,
          // default : ""
          // required:true,
      },
      country:{
        type:String,
        // default : ""
      },
      telephone:{
        type:String,
        // default : ""
      },
      
      state:{
        type:String,
        // default : ""
      },
      dateOfBirth:{
        type:String,
        // default : ""
      },
      occupation:{
        type:String,
        // default : ""
      },
      date:{
          type:Date,
          default:Date.now
      }, 
      updated:{
        type:Date
      }
    }
    
    
});


function validateUser(user){
  const schema = {
      username:Joi.string().min(5).max(50).required(),
      email: Joi.string().min(5).max(255).required().email(),
      password: Joi.string().min(5).max(255).required(),
      confirmPassword: Joi.any().valid(Joi.ref('password')).required().options({ language: { any: { allowOnly: 'must match password' } } })
  }
  return Joi.validate(user, schema)
}


module.exports ={
  User : mongoose.model('signupuser', SignupUserSchema),
  validateUser:validateUser
}

















