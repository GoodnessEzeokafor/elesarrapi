const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

const FeedbackSchema = new Schema({   

    email:{
        type:String, 
        required:true    
    },

    firstName:{
        type:String, 
        required:true    
    }, 

    lastName:{
        type:String, 
        required:true    
    },

    text:{
        type:String, 
        required:true
    },

    date_created:{
        type:Date,
        default:Date.now
    },

    date_updated:{
        type:Date
    }
})


function validateFeedback(feedback){
    const schema={
        email: Joi.string().min(5).max(255).required().email(),
                firstName:Joi.string().min(5).max(120).required(),
        lastName:Joi.string().min(5).max(120).required(),
        text:Joi.string().min(5).max(300).required()
    }
    return Joi.validate(feedback, schema)
}
// const Comment = mongoose.model('Comment', commentSchema);
// module.exports = Comment

module.exports = {
    Feedback : mongoose.model("feedback", FeedbackSchema),
    validateFeedback:validateFeedback
}
// module.exports = Feedback = mongoose.model("feedback", FeedbackSchema)
