const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

const guestSchema = new mongoose.Schema({

    full_name: {
        type: String,
        required: true,
    },

    email: {
        type: String,
        required: true,
    },

    amount: {
        type: Number,
        required: true,
    },

    projectID :{
        type: String,
        // type:Schema.Types.ObjectId,
        // ref:'ProjectSchema'
    },

    reference: {
        type: String,
        required: true
    },

    datePaid:{
        type:Date,
        default:Date.now
    }
    
});


function validateGuest(guest){
    const schema={
        full_name:Joi.string().min(5).max(120).required(),
        email: Joi.string().min(5).max(255).required().email(),
                amount:Joi.number(),
        projectID:Joi.objectId().required(),
        reference:Joi.string().min(5).max(120).required()
    }
    return Joi.validate(guest, schema)
}

module.exports = {
    Guest : mongoose.model('Guest', guestSchema),
    validateGuest:validateGuest
}
// const Guest = mongoose.model('Guest', guestSchema);
// module.exports = Guest