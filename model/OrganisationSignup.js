/**
 * 
 * @Developer - Goodness Chinemerem Ezeokafor
 * @position - CEO/LEAD DEVELOPER
 * @model - OrganisationUserSchema
 * @DocumentationDate - 21st June 2020
 * @fields - 
 * 
 *          - IS ORGANISATION
 *              @type Boolean
 *              @required true
 * 
 *          -  NAME OF ORGANISATION
 *              @type Number
 *              @default 0 
 * 
 *          -  EMAIL
 *              @type STRING
 * 
 *          - TELEPHONE NO
 *              @type STRING 
 * 
 *          - CLASS
 *              @type STRING
 * 
 *          - OFFICE ADDRESS
 *              @type STRING 
 * 
 *  *       - RC NO
 *              @type STRING 
 * 
 * 
 *          - LOCATION OF COMPANY
 *              @type String
 *              @required true
 * 
 *          - COUNTRY
 *              @type String
 *              @required true
 * 
 *          - STATE
 *              @type String
 *              @required true
 *              
 * 
 * 
 *  *          - POSTAL CODE 
 *              @type String
 *              @required true
 *          
 * 
 * 
 * 
 *  *  *          - VERIFIED 
 *                  @type String
 *                  @required true
 * 
 * 
 *  *  *  *       - DATE 
 *                  @type DATE
 *                  @required true
 * 
 *  *  *  *  *   - DATE UPDATED
 *                  @type DATE
 *                  @required true
 * 
 */

const mongoose = require('mongoose')
const Schema = mongoose.Schema;


//Create Schema
const OrganisationUserSchema = new Schema({

    isOrganization : {
        type : Boolean,
      default : true
        
      },
    name_of_organisation:{
        type:String,
        required:true,
    },
    email:{
        type:String, 
        required:true,
    },
    telephone_no:{
        type:String,
        required:true
    },
    class : {
        type :String,
        default : "Organisation"
      },
    office_address:{
        type:String, 
        required:true
    },
    rc_no:{
        type:String, 
        required:true
    },
    location_of_company:{
        type:String,
        required:true  
    },
    payments : {
        type : Array,
        default : []
    },
    country:{
        type:String,
        required:true
    },
    state:{
        type:String,
        required:true
    },
    password: {
        type: String,
        required: true,
        min: [6, 'Password Too short to be a mail'],
      },
    postal_code:{
        type:String,
        required:true
    },
    date:{
        type:Date,
        default:Date.now
    },
    updated:{
        type:Date
        // default:Date.now
    },
    verified : {
        type : Boolean,
        default : false
    }
})
module.exports =OrganisationProfile = mongoose.model("OrganisationUserSchema", OrganisationUserSchema)
