const mongoose = require('mongoose')
const Schema = mongoose.Schema;


const BadgeSchema = new Schema({   
    
    user_email:{
        type:String, 
        required:true    
    },

    // user_id:{
    //     type:Schema.Types.ObjectId,
    //     ref:'signupuser'
    // },
    
    badgeurl:{
        type:String, 
        required:true
    },
    
    status:{
        type:String
    },
    // svgcode:{
    //     type:string
    // },
    
    date_created:{
        type:Date,
        default:Date.now
    },
    
    date_updated:{
        type:Date
    }
})

module.exports = Badge = mongoose.model("badge", BadgeSchema)
