const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)


const slugify = require('slugify')

const ProjectSchema = new Schema({
    
    project_name:{
        type:String, 
        required:true,
    },

    slug:{
        type:String
    },

    userID:{
        type:Schema.Types.ObjectId,
        ref:'signupuser'
    },

    project_description:{
        type:String, 
        required:true
    },


    image_slide_2:{
        type:String,
    },

    image_slide_3:{
        type:String,
    },
    

    fb_url:{
        type:String
    },

    linkedin:{
        type:String
    },

    inst_url:{
        type:String
    },

    video_url:{
        type:String,
        // default:''
    },

    project_deadline:{
        type:Date,
        required:true
    },

    date_created:{
        type:Date,
        default:Date.now
    },

    project_goal:{
        type:String, 
        required:true
    },

    project_current_balance:{
        type:Number,
        default:0
    },


    category:{
        type:Schema.Types.ObjectId,
        ref:'category'
    },

    completedAt:{
        type:Date,
        default:0
    },


    like:[{
        type:Schema.Types.ObjectId,
        ref:'signupuser'
    }],

    useCrypto:{
        type:Boolean,
        default:false
    },

    updates:[{
        type:Schema.Types.ObjectId,
        ref:'update'
    }],

    project_comments:[{
        type:Schema.Types.ObjectId,
        ref:'project_comments'
    }],

    faq:[{
        type:Schema.Types.ObjectId,
        ref:'project_faq'
    }],
    status:{
        type:String,
        default:"draft"
    },
    completed:{
        type:Boolean,
        default:false
    },

    
    date_updated:{
        type:Date,
        default:''
    },


    flag:{
        type:Boolean,
        default:false
    },

    verified:{
        type:Boolean,
        default:false
    }
})


ProjectSchema.pre("save", function(next) {
    slug = slugify(this.project_name);
    this.slug=slug.toLowerCase() 
    next();
});


function validateProject(project){
    const schema = {
        project_name:Joi.string().min(5).max(300).required(),
        // slug:Joi.string().min(5).max(300).required(),
        // userID:Joi.objectId().required(),
        project_description: Joi.string(),
        fb_url:Joi.string(),
        linkedin:Joi.string(),
        inst_url:Joi.string(),
        video_url:Joi.string(),
        project_deadline:Joi.date().required(),
        project_goal:Joi.number().required(),
        category:Joi.objectId().required(),
    }
    return Joi.validate(project, schema)
}



function validateProjectImages(project){
    const schema = {
        image_slide_2:Joi.string(),
        image_slide_3:Joi.string(), 
   }
    return Joi.validate(project, schema)
}



module.exports = {
    Project : mongoose.model("ProjectSchema", ProjectSchema),
    validateProject:validateProject,
    validateProjectImages:validateProjectImages
}



