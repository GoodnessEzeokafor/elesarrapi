const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

const projectCommentSchema = new mongoose.Schema({
    
    content: {
        type: String,
        required: true,
    },

    likes: {
        type: Number,
        default: '0',
    },

    project:{
        type:Schema.Types.ObjectId,
        ref:'ProjectSchema'
    },

    owner:{
        type:Schema.Types.ObjectId,
        ref:'signupuser'
    },

    dateCreated:{
        type:Date,
        default:Date.now
    },

    dateUpdated:{
        type:Date
    }
    
});


function validateProjectComment(comment){
    const schema={
        content:Joi.string().min(5).max(120).required(),
        likes:Joi.number(),
        project:Joi.objectId().required()
    }
    return Joi.validate(comment, schema)
}
// const Comment = mongoose.model('Comment', commentSchema);
// module.exports = Comment

module.exports = {
    ProjectComment : mongoose.model('project_comments', projectCommentSchema),   
    validateProjectComment:validateProjectComment
}

