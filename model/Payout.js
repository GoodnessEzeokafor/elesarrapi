const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)


const PayoutSchema = new Schema({   
    
    email:{
        type:String, 
        // required:true    
    },
    
    amount:{
        type:String, 
        // required:true    
    },
    
    userID:{
        type:Schema.Types.ObjectId,
        ref:'signupuser'

    },
    
    projectID :{
        type:String
    },
    
    bankName:{
        type:String, 
        // required:true
    },
    
    accountNO:{
        type:String, 
        // required:true
    },

    bankCode:{
        type:String, 
        // required:true
    },

    date_created:{
        type:Date,
        default:Date.now
    },

    date_updated:{
        type:Date
    }
})



function validatePayout(payout){
    const schema = {
        email: Joi.string().min(5).max(255).required().email(),
        // amount:Joi.number().required(),        
        // userID:Joi.objectId().required(),
        projectID:Joi.string().required(),
        bankName:Joi.string().min(5).max(100).required(),
        accountNO:Joi.string().min(10).max(10).required(),
        // bankCode:Joi.string().min(3).max(6).required()
        
   }

   return Joi.validate(payout, schema)
}

module.exports = {
    Payout : mongoose.model("payout", PayoutSchema),
    validatePayout:validatePayout
}

// module.exports = Payout = mongoose.model("payout", PayoutSchema)
