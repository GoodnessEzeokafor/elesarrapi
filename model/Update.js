const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

const updateSchema = new mongoose.Schema({
    content: {
        type: String,
        required: true,
    },
    projectID:{
        type:Schema.Types.ObjectId,
        ref:'ProjectSchema'
    },
    userID:{
        type:Schema.Types.ObjectId,
        ref:'signupuser'
    },
    commentUpdate:[{
        type:Schema.Types.ObjectId,
        ref:'update_comments'
    }],
    dateCreated:{
        type:Date,
        default:Date.now
    },
    dateUpdated:{
        type:Date
    }
});


function validateUpdate(update){
    const schema = {
        content:Joi.string().min(5).max(500).required(),
        projectID: Joi.objectId().required(),
        // userID: Joi.objectId().required()
        // confirmPassword: Joi.any().valid(Joi.ref('password')).required().options({ language: { any: { allowOnly: 'must match password' } } })
    }
    return Joi.validate(update, schema)
  
  }
  
module.exports ={
    Update :mongoose.model('update', updateSchema),
    validateUpdate:validateUpdate
}
