/**
 * 
 * @Developer - Goodness Chinemerem Ezeokafor
 * @position - CEO/LEAD DEVELOPER
 * @model - Item
 * @DocumentationDate - 21st June 2020
 * @fields - 
 * 
 * 
 *  MODEL NOT IN USE RIGHT NOW

 */

const mongoose = require('mongoose')
const Schema = mongoose.Schema;


//Create Schema
const ItemSchema = new Schema({
    name:{
        type:String,
        required:true,
    },
    date:{
        type:Date,
        default:Date.now
    }
})

module.exports =Item = mongoose.model("Item", ItemSchema)
