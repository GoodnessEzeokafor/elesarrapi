const mongoose = require('mongoose')
const Schema = mongoose.Schema;


//Create Schema
const ProfileSchema = new Schema({
    
    firstName:{
        type:String,
        required:true,
    },

    lastName:{
        type:String,
        required:true,
    },

    nationalIdentityCardNumber:{
        type:String,
        required:true,
        unique:true
    },

    address:{
        type:String,
        required:true,
    },

    profilePicture:{
        type:String,
        required:false
    },
    
    user:{
        type:Schema.Types.ObjectId,
        ref:'signupuser'
    },
    date:{
        type:Date,
        default:Date.now
    }
})

module.exports =Profile = mongoose.model("Profile", ProfileSchema)




